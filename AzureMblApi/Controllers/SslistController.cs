﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using AzureMblApi.Models;

namespace AzureMblApi.Controllers
{
    public class SslistController : ApiController
    {
        public class ssdata
        {
            public string skkcode { get; set; }
            public string ssname { get; set; }
            public string wrgtype { get; set; }
            public string wrgtypeeng { get; set; }
        }
        //HttpGet api/Sslist/?skkcode="SKKCODE"&wrgno="
        public string Get(string skkcode, string wrgno)
        {
            string data = "";
            string jsonstr = "[{\"skkcode\"; null, \"ssname\":null}]";
            try
            {
                List<ssdata> sslist = new List<ssdata>();
                if (skkcode.Length <= 10)
                {
                    if (GlobalVar.testmode == true)
                    {
                        if (skkcode == "M114000009")
                        {
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                        }
                        else if (skkcode == "M114000010")
                        {
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                        }
                        else if (skkcode == "M114000011")
                        {
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                        }
                    }
                    SiraPtlSSInf ssinf = new SiraPtlSSInf();
                    ssinf.OpenTableSkkcode(skkcode, wrgno, "*");
                    int num = ssinf.GetNumOfRecord();
                    if( num > 0)
                    {
                        for (int i = 0; i < num; i++)
                        {
                            ssdata newss = new ssdata();
                            newss.skkcode = ssinf.GetSkkcodeByLine(i);
                            newss.ssname = ssinf.GetSiteNameByLine(i);
                            ( string wrg, string wrgeng ) = ssinf.GetSiteWrgTypeByLine(i);
                            newss.wrgtype = wrg;
                            newss.wrgtypeeng = wrgeng;
                            if( GlobalVar.testmode == true)
                            {
                                switch(i)
                                {
                                    case 0:
                                        newss.wrgtype = "減";
                                        newss.wrgtypeeng = "Low";
                                        break;
                                    case 1:
                                        newss.wrgtype = "満";
                                        newss.wrgtypeeng = "Full";
                                        break;
                                    case 2:
                                        newss.wrgtype = "水検知";
                                        newss.wrgtypeeng = "Water";
                                        break;
                                    case 3:
                                        newss.wrgtype = "センサ異常";
                                        newss.wrgtypeeng = "Sensor";
                                        break;
                                    case 4:
                                        newss.wrgtype = "漏えい";
                                        newss.wrgtypeeng = "Leak Test";
                                        break;
                                    case 5:
                                        newss.wrgtype = "リーク";
                                        newss.wrgtypeeng = "Leak";
                                        break;
                                    case 6:
                                        newss.wrgtype = "データ更新異常";
                                        newss.wrgtypeeng = "No data";
                                        break;
                                    case 7:
                                        newss.wrgtype = "減,水検知";
                                        newss.wrgtypeeng = "Low,Water";
                                        break;
                                    case 8:
                                        newss.wrgtype = "センサ異常,漏えい";
                                        newss.wrgtypeeng = "Sensor,Leak Test";
                                        break;
                                    case 9:
                                        newss.wrgtype = "リーク,データ更新異常";
                                        newss.wrgtypeeng = "Leak,No data";
                                        break;
                                    case 10:
                                        newss.wrgtype = "減,水検知,センサ異常";
                                        newss.wrgtypeeng = "Low,Water,Sensor";
                                        break;
                                    case 11:
                                        newss.wrgtype = "漏えい,リーク,データ更新異常";
                                        newss.wrgtypeeng = "Leak Test,Leak,No Data";
                                        break;
                                    case 12:
                                        newss.wrgtype = "減,水検知,センサ異常,漏えい,リーク";
                                        newss.wrgtypeeng = "Low,Water,Sensor,Leak";
                                        break;
                                }
                            }
                            sslist.Add(newss);
                        }
                        jsonstr = JsonSerializer.Serialize(sslist);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (GlobalVar.testmode == true)
                {
                    if (skkcode == "M114000009")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                    }
                    else if (skkcode == "M114000010")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                    }
                    else if (skkcode == "M114000011")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                    }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return jsonstr;
        }

    }
}
