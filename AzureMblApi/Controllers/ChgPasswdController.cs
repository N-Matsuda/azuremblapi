﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Web.Helpers;
using System.Security.Claims;
using System.Security.Cryptography;
using AzureMblApi.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class ChgPasswdController : ApiController
    {
        public class AuthResult
        {
            public string result { get; set; }
            public string skkcode { get; set; }
        }
        //private readonly RandomNumberGenerator _rng;
        // GET api/ChgPasswd/?opasswd=xxxxx&npasswd=xxxxx
        public string Get(string email, string npasswd)
        {
            AuthResult authres = new AuthResult();
            authres.result = "NG";
            authres.skkcode = "None";
            string orghash = "";
            string sqlstr = "SELECT PasswordHash FROM AspNetUsers WHERE Email = '" + email + "'";
            string cnstr = GlobalVar.DBCONNECTION;
            string jsonstr = "";
            try
            {
                //SQL参考
                //https://garafu.blogspot.com/2016/05/cs-sqlserver-exec-select.html
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        orghash = reader["PasswordHash"] as string;
                        break;
                    }
                }
                cn.Close();
                //string hashed = Crypto.HashPassword(password);
                string newhash = PasswordHash.ChangePassword(orghash, npasswd);
                if( newhash == "" )
                {
                    jsonstr = JsonSerializer.Serialize(authres);
                    return jsonstr;
                }

                //新パスワード登録
                sqlstr = "UPDATE AspNetUsers SET PasswordHash ='" + newhash + "' WHERE Email = '" + email + "'"; 
                cn.Open();
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        orghash = reader["PasswordHash"] as string;
                        break;
                    }
                }
                cn.Close();
                //対応SKKコード取り出し
                authres.result = "OK";
                sqlstr = "SELECT SKKコード FROM ANUserProfile WHERE メールアドレス = '" + email + "'";
                cn.Open();
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        string skkcode = reader["SKKコード"] as string;
                        authres.skkcode = skkcode.TrimEnd();
                        break;
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
            }
            jsonstr = JsonSerializer.Serialize(authres);
            return jsonstr;
        }
    }
}
