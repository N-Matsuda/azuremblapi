﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AzureMblApi.Models;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class ReqpinmailController : ApiController
    {
        static string NOACCEMAIL = "no account email";
        static string FAILEDMAIL = "mail failed";
        static string PARAMERR = "parameter error";
        public class ResultCode
        {
            public string result { get; set; }
            public string cause { get; set; }
        }
        public string GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            string str = (_rdm.Next(_min, _max)).ToString("####");
            return str;
        }
        //private readonly RandomNumberGenerator _rng;
        // GET api/Reqpinmail/?email=xxxxx&acemail=xxxxx&lang=xxxxx
        public string Get(string email, string acemail, string lang)
        {
            string jsonstr = "[{\"result\"; null, \"cause\":null}]";
            ResultCode rscode = new ResultCode();

            //新パスワードが登録済か確認
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            //DB登録数をカウント
            string sqlstr = "SELECT COUNT(*) FROM AspNetUsers WHERE Email = '" + acemail + "'";
            System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            int count = (int)Com.ExecuteScalar();
            if (count == 0)
            {
                rscode.result = "NG";
                rscode.cause = NOACCEMAIL;
                cn.Close();
                jsonstr = JsonSerializer.Serialize(rscode);
                return jsonstr;
            }
            cn.Close();

            if ((lang.StartsWith("Eng") == true) || (lang.StartsWith("Jap") == true))
            {
                try
                {   //PINコードメール送信
                    string pincode = GenerateRandomNo();
                    string message = "";
                    if( lang.StartsWith("Eng") == true)
                    {
                        message = "Your PIN code for password change is:\r\n";
                        message += pincode + "\r\n\r\n";
                        message += "The above PIN code is to change password for [" + GlobalVar.appnameeng + "] only.";
                        message += "Please enter the code in [" + GlobalVar.appnameeng + "].\r\n\r\n";
                        message += "Finish the above procedure within 24 hours.\r\n";
                        message += "You cannnot change password until finishing authorization by PIN code.\r\n";
                        message += "Please note that This mail is sent from send only address, and can not accept a return mail.\r\n";
                    } else
                    {
                        message = "■PINコード 【パスワード変更】\r\n";
                        message += pincode + "\r\n\r\n";
                        message += "こちらは「" + GlobalVar.appname + "」パスワード変更用のPINコードです。\r\n";
                        message += "アプリにてPINコードを入力してください。\r\n\r\n";
                        message += "※登録受付時より24時間以内に認証を完了してください。\r\n";
                        message += "※認証を完了させるまで、パスワードの変更は完了できません。\r\n";
                        message += "※このメールは送信専用のアドレスから配信していますので、返信などは受け付けておりません。\r\n";
                    } 
                    SendMail sendml = new SendMail();
                    sendml.SendMailExec(message,email,lang);
                    //DBにPINコードを登録 pincode + ":" + "yyyy/MM/dd HH:mm"　ANUserProfile デバイストークンの所に仮登録する。
                    DateTime dt = DateTime.Now.ToLocalTime();
                    sqlstr = "UPDATE ANUserProfile SET デバイストークン ='" + pincode + ":" + dt.ToString("yyyy/MM/dd HH:mm") + "' WHERE メールアドレス='" + acemail + "'";
                    DBCtrl.ExecNonQuery(sqlstr);
                    rscode.result = "OK";
                }
                catch (Exception ex)
                {
                    rscode.result = "NG";
                    rscode.cause = FAILEDMAIL;
                    jsonstr = JsonSerializer.Serialize(rscode);
                    return jsonstr;
                }
            } else
            {
                rscode.result = "NG";
                rscode.cause = PARAMERR;
            }
            jsonstr = JsonSerializer.Serialize(rscode);
            return jsonstr;

        }

    }
}
