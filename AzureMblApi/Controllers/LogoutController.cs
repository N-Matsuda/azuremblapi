﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Helpers;
using System.Security.Claims;
using System.Security.Cryptography;
using AzureMblApi.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class LogoutController : ApiController
    {
        public class AuthResult
        {
            public string result { get; set; }
        }
        //GET api/Logout/?email=xxxx
        public string Get(string email)
        {
            AuthResult authres = new AuthResult();
            authres.result = "OK"; //emailの登録がなくてもOKを返す。
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();

                //DB登録数をカウント
                string sqlstr = "SELECT COUNT(*) FROM ANUserProfile WHERE メールアドレス = '" + email + "'";
                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                int count = (int)Com.ExecuteScalar();
                if( count > 0 )
                {
                    //ログインステータス登録
                    sqlstr = "UPDATE ANUserProfile SET ログインステータス = '-' WHERE メールアドレス = '" + email + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                    authres.result = "OK";
                } 
                cn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
            }
            string jsonstr = JsonSerializer.Serialize(authres);
            return jsonstr;
        }
    }
}
