﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using AzureMblApi.Models;

namespace AzureMblApi.Controllers
{
    public class SchssController : ApiController
    {
        public class ssdata
        {
            public string skkcode { get; set; }
            public string ssname { get; set; }
            public string wrgtype { get; set; }
            public string wrgtypeeng { get; set; }
        }
        //HttpGet api/Schss/?skkcode="SKKCODE"&chars="
        public string Get(string skkcode, string chars)
        {
            string data = "";
            string jsonstr = "[{\"skkcode\"; null, \"ssname\":null}]";
            try
            {
                if (GlobalVar.testmode == true)
                {
                    if (chars == "3")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                    }
                    else if (chars == "4")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                    }
                    else if (chars == "5")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                    }
                }

                //string schwrd = ToShiftJis(chars);
                List<ssdata> sslist = new List<ssdata>();
                if (skkcode.Length <= 10)
                {
                    SiraPtlSSInf ssinf = new SiraPtlSSInf();
                    ssinf.OpenTableSkkcode(skkcode, "*", chars);
                    int num = ssinf.GetNumOfRecord();
                    if (num > 0)
                    {
                        for (int i = 0; i < num; i++)
                        {
                            ssdata newss = new ssdata();
                            newss.skkcode = ssinf.GetSkkcodeByLine(i);
                            newss.ssname = ssinf.GetSiteNameByLine(i);
                            (string wrg, string wrgeng) = ssinf.GetSiteWrgTypeByLine(i);
                            newss.wrgtype = wrg;
                            newss.wrgtypeeng = wrgeng;
                            sslist.Add(newss);
                        }
                        jsonstr = JsonSerializer.Serialize(sslist);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (GlobalVar.testmode == true)
                {
                    if (chars == "3")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                    }
                    else if (chars == "4")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                    }
                    else if (chars == "5")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                    }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return jsonstr;
        }
        private string ToShiftJis(string unicodeStrings)
        {
            var unicode = Encoding.Unicode;
            var unicodeByte = unicode.GetBytes(unicodeStrings);
            var s_jis = Encoding.GetEncoding("shift_jis");
            var s_jisByte = Encoding.Convert(unicode, s_jis, unicodeByte);
            var s_jisChars = new char[s_jis.GetCharCount(s_jisByte, 0, s_jisByte.Length)];
            s_jis.GetChars(s_jisByte, 0, s_jisByte.Length, s_jisChars, 0);
            return new string(s_jisChars);
        }
    }
}
