﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AzureMblApi.Controllers
{
    public class SoracomController : ApiController
    {
        public class SoracomReq
        {
            public string Payload { get; set; }
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        public void Post(SoracomReq Req)
        {
            string payload = Req.Payload;
            if( payload.Length <= 128)
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                string timedt = dt.ToString("yyyyMMddHHmm");
                string sqlstr = "INSERT INTO SoracomData (日付,データ) VALUES ('" + timedt + "','" + payload + "')";
                Models.DBCtrl.ExecNonQuery(sqlstr);
            }
        }


    }
}
