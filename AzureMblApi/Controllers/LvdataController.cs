﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AzureMblApi.Models;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class LvdataController : ApiController
    {
        protected static string WTRINCPRIV = "WTR"; //水情報表示権限
        public class TankDetail
        {
            public string tankno { get; set; } //タンク番号
            public string oiltype { get; set; } //液種
            public string oiltypeeng { get; set; } //液種(English)
            public string oilvol { get; set; } //液量
            public string wrgstr { get; set; } //警報
            public string wrgeng { get; set; } //警報(English)
            public string capa { get; set; }　//全容量
            public string oilper { get; set; } //在庫率
            public string wtrlev { get; set; } //水位
            public string wtrvol { get; set; } //水量
            public string oillev { get; set; } //液位
            public string dlvvol { get; set; } //荷卸し量 (1000リットル単位)
            public string salesvol { get; set; } //販売量
        }
        public class Lvdata
        {
            public string date { get; set; }
            public string skkcode { get; set; }
            public string extrainf { get; set; }
            public List<TankDetail> tdetaillst { get; set; }
        }

        //[HttpGet("{id}")] Get api/Lvdata/?skkcode="SKKCODE"
        //=>Get api/Lvdata/?skkcode="SKKCODE"&email=
        //public string Get(string skkcode)
        public string Get(string skkcode, string email)
        {
            string data = "";
            if (skkcode.Length == 10)
            {
                //email登録チェック
                string priv = "";
                bool bwtrinc = false; //水情報を含めるか？
                bool bfound = false;
                string sqlstr = "SELECT 権限 FROM ANUserProfile WHERE メールアドレス = '" + email + "'";
                string cnstr = GlobalVar.DBCONNECTION;
                try
                {
                    System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                    cn.Open();
                    System.Data.SqlClient.SqlCommand Com;
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);

                    //指定されたemailアドレスが登録されているか確認
                    using (var reader = Com.ExecuteReader())
                    {
                        while (reader.Read() == true)
                        {
                            priv = reader["権限"] as string;
                            bfound = true;
                            break;
                        }
                    }
                    cn.Close();
                    //指定されたemailアドレスで水情報の表示が許可されているか確認
                    if ((bfound == true) && (priv != null) )
                    {
                        if (priv.IndexOf(WTRINCPRIV) >= 0)
                            bwtrinc = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }

                string scode = skkcode.Substring(0, 10);
                MRouterTable mrt = new MRouterTable();
                mrt.OpenTableSkkcode(scode);
                SIRASite srsite = new SIRASite();
                srsite.OpenTableSkkCode(scode);

                if (mrt.GetNumOfRecord() > 0)
                {
                    try
                    {
                        if( GlobalVar.testmode == true )
                        {
                            if (skkcode == "M114000009")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                            }
                            else if (skkcode == "M114000010")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                            }
                            else if (skkcode == "M114000011")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                            }
                        }
                        data = mrt.GetZaikoString();
                        Lvdata lvdat = new Lvdata();
                        lvdat.date = "20" + data.Substring(0, 10);
                        lvdat.skkcode = scode;
                        ZaikoStr zs = new ZaikoStr(data);
                        zs.Analyze();
                        if( zs.bwtrinf == true)
                        {
                            lvdat.extrainf = "true";
                        } else
                        {
                            lvdat.extrainf = "false";
                        }
                        List<TankDetail> tdetail = new List<TankDetail>();
                        int numtank = zs.numtank;
                        string tnostr = zs.tanknolst[numtank-1].TrimStart('0');
                        int tnomax = 0;
                        if (int.TryParse(tnostr, out tnomax) == true)
                            numtank = tnomax;
                        //i=配列番号, j=タンク番号に相当
                        for (int i = 0, j=0; j < numtank;)
                        {
                            TankDetail tdl = new TankDetail();
                            tdl.tankno = zs.tanknolst[i].TrimStart('0');
                            int lqtype;
                            bool res = int.TryParse(zs.lqtypelst[i], out lqtype);
                            if (res == true)
                            {
                                tdl.oiltype = OilName.GetOilName(int.Parse(zs.lqtypelst[i]));
                                tdl.oiltypeeng = OilName.GetOilNameEng(int.Parse(zs.lqtypelst[i]));
                            } else
                            {
                                tdl.oiltype = "不明";
                                tdl.oiltypeeng = "unknown";
                            }
                            string oilvol = zs.vollst[i];
                            string lowvol = zs.lowlmtlst[i];
                            string strwk = zs.sslan2stat[i];
                            string strwk2 = zs.sslanstat[i];
                            //警報文字列取得
                            (string sts, string stseng) = GetWarning.GetWrngStr(strwk, strwk2);
                            if(GlobalVar.testmode==true)
                            {
                                if( skkcode == "M100020003" )
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            sts = "減";
                                            stseng = "Low";
                                            break;
                                        case 1:
                                            sts = "満";
                                            stseng = "Full";
                                            break;
                                        case 2:
                                            sts = "水検知";
                                            stseng = "Water";
                                            break;
                                        case 3:
                                            sts = "センサ異常";
                                            stseng = "Sensor";
                                            break;
                                        case 4:
                                            sts = "漏えい";
                                            stseng = "Leak Test";
                                            break;
                                        case 5:
                                            sts = "リーク";
                                            stseng = "Leak";
                                            break;
                                        case 6:
                                            sts = "データ更新異常";
                                            stseng = "No data";
                                            break;
                                        case 7:
                                            sts = "減,水検知";
                                            stseng = "Low,Water";
                                            break;
                                        case 8:
                                            sts = "センサ異常,漏えい";
                                            stseng = "Sensor,Leak Test";
                                            break;
                                        case 9:
                                            sts = "リーク,データ更新異常";
                                            stseng = "Leak,No data";
                                            break;
                                        case 10:
                                            sts = "減,水検知,センサ異常";
                                            stseng = "Low,Water,Sensor";
                                            break;
                                    }
                                }
                            }
                            tdl.wrgstr = sts;
                            tdl.wrgeng = stseng;

                            //string capa = zs.capalst[i].TrimStart('0');
                            int ioilvol = 0;
                            int icapa = srsite.GetTankCapa(0, j + 1);
                            if( icapa == 0)
                            {
                                SiteInf stinf = new SiteInf();
                                stinf.OpenTableSkkcode(scode);
                                icapa = stinf.GetCapacityByTank(j + 1);
                            }
                            if ( res = int.TryParse(oilvol, out ioilvol))
                            {
                            // if( res = int.TryParse(capa, out icapa))
                            // {
                                    try
                                    {
                                        double per = (double)ioilvol / (double)icapa;
                                        tdl.oilper = per.ToString("P1");
                                    } catch(Exception ex)
                                    {
                                        Console.WriteLine(ex.ToString());
                                    }
                            //    }
                            }
                            int ilowvol = 0;
                            if( res = int.TryParse(lowvol, out ilowvol))
                            {
                                if( ioilvol < ilowvol )
                                {
                                    if( tdl.wrgstr != "通常")
                                    {
                                        tdl.wrgstr += ",";
                                        tdl.wrgeng += ",w";
                                    }
                                    tdl.wrgstr += "減";
                                    tdl.wrgeng += "Low";
                                }
                            }

                            if (zs.bwtrinf == true)
                            {
                                if( tdl.wrgeng.IndexOf("Sensor") >= 0 )
                                {

                                } else
                                {

                                }
                                //液位
                                int oilvl = 0;
                                res = int.TryParse(zs.oillvllst[i], out oilvl);
                                if (res == true)
                                {
                                    oilvl = oilvl / 100;
                                    tdl.oillev = oilvl.ToString("##,#") + "mm";
                                } else
                                    tdl.oillev = "0mm";

                                //水位 本来下記仕様
                                //検知開始水位  X-2→25mm 
                                //              X-1二重殻タンク→40ｍｍ 
                                //              X-1一重殻タンク→45ｍｍ
                                //暫定で40mmにする。2020/06/16
                                //水位、水量指定
                                if( (bwtrinc == true ) && ( tdl.wrgeng.IndexOf("Sensor") < 0 ) ) //水位を表示するとき
                                {

                                    int wtrlvl = 0;
                                    res = int.TryParse(zs.wtrlvllst[i], out wtrlvl);
                                    if ((res == true) && (wtrlvl > 4000))
                                    {
                                        wtrlvl = wtrlvl / 100;
                                        tdl.wtrlev = wtrlvl.ToString("##,#") + "mm";
                                    }
                                    else
                                    {
                                        tdl.wtrlev = "0mm";
                                        wtrlvl = 0;
                                    }
                                    //水量
                                    int wtrvol = 0;
                                    res = int.TryParse(zs.wtrvollst[i], out wtrvol);
                                    if ((res == true) && (wtrvol > 0) && (wtrlvl > 0))
                                    {
                                        wtrvol = wtrvol / 100;
                                        tdl.wtrvol = wtrvol.ToString("##,#") + "L";
                                    }
                                    else
                                        tdl.wtrvol = "0L";
                                } else
                                {
                                    tdl.wtrlev = "999mm";
                                    tdl.wtrvol = "0L";
                                }
                                //荷卸し量
                                int dvol;
                                res = int.TryParse(zs.dlvvollst[i], out dvol);
                                if( (res == true) && ( dvol > 0) )
                                {
                                    dvol = dvol * 1000;
                                    //数値に","を入れる
                                    tdl.dlvvol = dvol.ToString("##,#") + "L";
                                }
                                else
                                {
                                    tdl.dlvvol = "0L";
                                }
                                //販売量
                                int svol;
                                res = int.TryParse(zs.salesvollst[i], out svol);
                                if( ( res == true ) && (svol > 0 ) )
                                {
                                    tdl.salesvol = svol.ToString("##,#") + "L";
                                } else
                                {
                                    tdl.salesvol = "0L";
                                }
                            }
                            //数値に","を入れる
                            tdl.oilvol = ioilvol.ToString("##,#") + "L";
                            //容量はkL単位
                            tdl.capa = (icapa / 1000).ToString() + "kL";
                            if (icapa > 0)
                            {
                                tdetail.Add(tdl);
                                i++;
                                j++;
                            } else
                            {
                                j++;
                            }
                        }
                        lvdat.tdetaillst = tdetail;
                        string jsonstr = JsonSerializer.Serialize(lvdat);
                        return jsonstr;
                    }
                    catch (Exception ex )
                    {
                        Console.WriteLine(ex.ToString());
                        if (GlobalVar.testmode == true)
                        {
                            if (skkcode == "M114000009")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                            }
                            else if (skkcode == "M114000010")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                            }
                            else if (skkcode == "M114000011")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                            } else
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                        }
                        else
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                    }
                } else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
            }
        }
    }
}
