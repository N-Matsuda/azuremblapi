﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AzureMblApi.Models;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class OildataController : ApiController
    {
        public class OilDetail
        {
            public string oiltype { get; set; } //液種
            public string oiltypeeng { get; set; } //液種(English)
            public string oilvol { get; set; } //液量
            public string capa { get; set; }　//全容量
            public string oilper { get; set; }
            public string dlvvol { get; set; } //荷卸し量 (1000リットル単位)
            public string salesvol { get; set; } //販売量
        }
        public class OilVolDetail
        {
            public int oilvol;
            public int capa;
            public int dlvvol;
            public int salesvol;
        }
        public class Lvdata
        {
            public string date { get; set; }
            public string skkcode { get; set; }
            public string extrainf { get; set; }
            public List<OilDetail> odetaillst { get; set; }
        }
        //[HttpGet("{id}")] Get api/Oildata/?skkcode="SKKCODE"
        public string Get(string skkcode)
        {
            string data = "";
            string jsonstr = "";
            if (skkcode.Length == 10)
            {
                string scode = skkcode.Substring(0, 10);
                MRouterTable mrt = new MRouterTable();
                mrt.OpenTableSkkcode(scode);
                //各油種別のクラスセット
                List<OilDetail> odetails = new List<OilDetail>();
                OilDetail RegularDtl = new OilDetail();
                RegularDtl.oiltype = "レギュラー";
                RegularDtl.oiltypeeng = "Unleaded";
                OilVolDetail RegVolDtl = new OilVolDetail();
                RegVolDtl.capa = RegVolDtl.oilvol = RegVolDtl.dlvvol = RegVolDtl.salesvol = 0;
                OilDetail PremDtl = new OilDetail();
                PremDtl.oiltype = "ハイオク";
                PremDtl.oiltypeeng = "Premium";
                OilVolDetail PremVolDtl = new OilVolDetail();
                PremVolDtl.capa = PremVolDtl.oilvol = PremVolDtl.dlvvol = PremVolDtl.salesvol = 0;
                OilDetail DieselDtl = new OilDetail();
                DieselDtl.oiltype = "軽油";
                DieselDtl.oiltypeeng = "Diesel";
                OilVolDetail DisVolDtl = new OilVolDetail();
                DisVolDtl.capa = DisVolDtl.oilvol = DisVolDtl.dlvvol = DisVolDtl.salesvol = 0;
                OilDetail KeroseneDtl = new OilDetail();
                KeroseneDtl.oiltype = "灯油";
                KeroseneDtl.oiltypeeng = "Kerosene";
                OilVolDetail KeroVolDtl = new OilVolDetail();
                KeroVolDtl.capa = KeroVolDtl.oilvol = KeroVolDtl.dlvvol = KeroVolDtl.salesvol = 0;
                OilDetail WasteoilDtl = new OilDetail();
                WasteoilDtl.oiltype = "廃油";
                WasteoilDtl.oiltypeeng = "Waste Oil";
                OilVolDetail WasteVolDtl = new OilVolDetail();
                WasteVolDtl.capa = WasteVolDtl.oilvol = WasteVolDtl.dlvvol = WasteVolDtl.salesvol = 0;

                if (mrt.GetNumOfRecord() > 0)
                {
                    try
                    {
                        if (GlobalVar.testmode == true)
                        {
                            if (skkcode == "M114000006")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                            }
                            else if (skkcode == "M114000007")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                            }
                            else if (skkcode == "M114000008")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                            }
                        }
                        data = mrt.GetZaikoString();
                        Lvdata lvdat = new Lvdata();
                        lvdat.date = "20" + data.Substring(0, 10);
                        lvdat.skkcode = scode;
                        ZaikoStr zs = new ZaikoStr(data); //現在の在庫データ分析
                        zs.Analyze();
                        List<OilDetail> tdetail = new List<OilDetail>();
                        SIRASite srsite = new SIRASite();
                        srsite.OpenTableSkkCode(scode);

                        int numtank = zs.numtank;
                        string tnostr = zs.tanknolst[numtank - 1].TrimStart('0');
                        int tnomax = 0;
                        if (int.TryParse(tnostr, out tnomax) == true)
                            numtank = tnomax;
                        //i=配列番号, j=タンク番号に相当
                        for (int i = 0,j=0; j < numtank;)
                        {
                            OilDetail tdl = new OilDetail();
                            int lqtype;
                            string oiltype = "";
                            string oiltypeeng = "";
                            bool res = int.TryParse(zs.lqtypelst[i], out lqtype);
                            if (res == true)
                            {
                                oiltype = OilName.GetOilName(int.Parse(zs.lqtypelst[i]));
                                oiltypeeng = OilName.GetOilNameEng(int.Parse(zs.lqtypelst[i]));
                            }
                            else
                            {
                                oiltype = "不明";
                                oiltypeeng = "unknown";
                            }
                            int oilvol = 0;
                            res = int.TryParse(zs.vollst[i], out oilvol);　//液量取り出し
                            if (res == false)
                                oilvol = 0;
                            //int capa = 0;
                            //res = int.TryParse(zs.capalst[i], out capa);　//容量値取り出し
                            //if (res == false)
                            //    capa = 0;
                            int capa = srsite.GetTankCapa(0, j + 1);
                            if (capa == 0)
                            {
                                SiteInf stinf = new SiteInf();
                                stinf.OpenTableSkkcode(scode);
                                capa = stinf.GetCapacityByTank(j + 1);
                            }
                            int dvol = 0;
                            int salesvol = 0;
                            if (zs.bwtrinf == true)　//荷卸し、販売量がある場合は取り出す
                            {
                                res = int.TryParse(zs.dlvvollst[i], out dvol);
                                if (res == true)
                                {
                                    dvol = dvol * 1000;
                                }
                                else
                                {
                                    dvol = 0;
                                }
                                res = int.TryParse(zs.salesvollst[i], out salesvol);
                                if (res == false)
                                    salesvol = 0;
                            }
                            if (capa > 0)
                            {
                                i++;
                                j++;
                            }
                            else
                            {
                                j++;
                            }
                            switch (oiltypeeng)　//各液種の各値に加算する。
                            {
                                case "Premium":
                                    PremVolDtl.capa += capa;
                                    PremVolDtl.oilvol += oilvol;
                                    PremVolDtl.dlvvol += dvol;
                                    PremVolDtl.salesvol += salesvol;
                                    break;
                                case "Unleaded":
                                    RegVolDtl.capa += capa;
                                    RegVolDtl.oilvol += oilvol;
                                    RegVolDtl.dlvvol += dvol;
                                    RegVolDtl.salesvol += salesvol;
                                    break;
                                case "Kerosene":
                                    KeroVolDtl.capa += capa;
                                    KeroVolDtl.oilvol += oilvol;
                                    KeroVolDtl.dlvvol += dvol;
                                    KeroVolDtl.salesvol += salesvol;
                                    break;
                                case "Diesel":
                                    DisVolDtl.capa += capa;
                                    DisVolDtl.oilvol += oilvol;
                                    DisVolDtl.dlvvol += dvol;
                                    DisVolDtl.salesvol += salesvol;
                                    break;
                                case "Wasteoil":
                                default:
                                    WasteVolDtl.capa += capa;
                                    WasteVolDtl.oilvol += oilvol;
                                    WasteVolDtl.dlvvol += dvol;
                                    WasteVolDtl.salesvol += salesvol;
                                    break;
                            }
                        }
                        //public string oilvol { get; set; } //液量
                        //public string capa { get; set; } //全容量
                        //public string dlvvol { get; set; } //荷卸し量 (1000リットル単位)
                        //public string salesvol { get; set; } //販売量

                        if (RegVolDtl.capa > 0) //レギュラーの液量などのセット
                        {
                            RegularDtl.oilvol = RegVolDtl.oilvol.ToString("##,#") + "L";
                            RegularDtl.capa = (RegVolDtl.capa/1000).ToString() + "kL";
                            RegularDtl.oilper = GetPerValue(RegVolDtl.oilvol, RegVolDtl.capa);
                            if (RegVolDtl.dlvvol > 0)
                                RegularDtl.dlvvol = RegVolDtl.dlvvol.ToString("##,#") + "L";
                            else
                                RegularDtl.dlvvol = "0L";
                            if (RegVolDtl.salesvol > 0)
                                RegularDtl.salesvol = RegVolDtl.salesvol.ToString("##,#") + "L";
                            else
                                RegularDtl.salesvol = "0L";
                            odetails.Add(RegularDtl);
                        }
                        if (PremVolDtl.capa > 0)　//ハイオクの液量などのセット
                        {
                            PremDtl.oilvol = PremVolDtl.oilvol.ToString("##,#") + "L";
                            PremDtl.capa = (PremVolDtl.capa/1000).ToString() + "kL";
                            PremDtl.oilper = GetPerValue(PremVolDtl.oilvol, PremVolDtl.capa);
                            if (PremVolDtl.dlvvol > 0)
                                PremDtl.dlvvol = PremVolDtl.dlvvol.ToString("##,#") + "L";
                            else
                                PremDtl.dlvvol = "0L";
                            if (PremVolDtl.salesvol > 0)
                                PremDtl.salesvol = PremVolDtl.salesvol.ToString("##,#") + "L";
                            else
                                PremDtl.salesvol = "0L";
                            odetails.Add(PremDtl);
                        }
                        if (DisVolDtl.capa > 0 )　//軽油の液量などのセット
                        {
                            DieselDtl.oilvol = DisVolDtl.oilvol.ToString("##,#") + "L";
                            DieselDtl.capa = (DisVolDtl.capa/1000).ToString() + "kL";
                            DieselDtl.oilper = GetPerValue(DisVolDtl.oilvol, DisVolDtl.capa);
                            if (DisVolDtl.dlvvol > 0)
                                DieselDtl.dlvvol = DisVolDtl.dlvvol.ToString("##,#") + "L";
                            else
                                DieselDtl.dlvvol = "0L";
                            if (DisVolDtl.salesvol > 0)
                                DieselDtl.salesvol = DisVolDtl.salesvol.ToString("##,#") + "L";
                            else
                                DieselDtl.salesvol = "0L";
                            odetails.Add(DieselDtl);
                        }
                        if (KeroVolDtl.capa > 0)　//灯油の液量などのセット
                        {
                            KeroseneDtl.oilvol = KeroVolDtl.oilvol.ToString("##,#") + "L";
                            KeroseneDtl.capa = (KeroVolDtl.capa/1000).ToString() + "kL";
                            KeroseneDtl.oilper = GetPerValue(KeroVolDtl.oilvol, KeroVolDtl.capa);
                            if (KeroVolDtl.dlvvol > 0)
                                KeroseneDtl.dlvvol = KeroVolDtl.dlvvol.ToString("##,#") + "L";
                            else
                                KeroseneDtl.dlvvol = "0L";
                            if (KeroVolDtl.salesvol > 0)
                                KeroseneDtl.salesvol = KeroVolDtl.salesvol.ToString("##,#") + "L";
                            else
                                KeroseneDtl.salesvol = "0L";
                            odetails.Add(KeroseneDtl);
                        }
                        if(WasteVolDtl.capa > 0)
                        {
                            WasteoilDtl.oilvol = WasteVolDtl.oilvol.ToString("##,#") + "L";
                            WasteoilDtl.capa = (WasteVolDtl.capa / 1000).ToString() + "kL";
                            WasteoilDtl.oilper = GetPerValue(WasteVolDtl.oilvol, WasteVolDtl.capa);
                            if (WasteVolDtl.dlvvol > 0)
                                WasteoilDtl.dlvvol = WasteVolDtl.dlvvol.ToString("##,#") + "L";
                            else
                                WasteoilDtl.dlvvol = "0L";
                            if (WasteVolDtl.salesvol > 0)
                                WasteoilDtl.salesvol = WasteVolDtl.salesvol.ToString("##,#") + "L";
                            else
                                WasteoilDtl.salesvol = "0L";
                            odetails.Add(WasteoilDtl);
                        }
                        if ((GlobalVar.testmode == true) && (skkcode == "M200000002"))
                        {
                            OilDetail AHOil = new OilDetail(); //No5
                            AHOil.oiltype = "A重油";
                            AHOil.oiltypeeng = "A Heavy Oil";
                            AHOil.oilvol = "5,000L";
                            AHOil.capa = "10kL";
                            AHOil.oilper = "50.0%";
                            AHOil.dlvvol = "0L";
                            AHOil.salesvol = "500L";
                            odetails.Add(AHOil);
                            OilDetail CHOil = new OilDetail(); //No6
                            CHOil.oiltype = "C重油";
                            CHOil.oiltypeeng = "C Heavy Oil";
                            CHOil.oilvol = "8,000L";
                            CHOil.capa = "20kL";
                            CHOil.oilper = "40.0%";
                            CHOil.dlvvol = "100L";
                            CHOil.salesvol = "400L";
                            odetails.Add(CHOil);
                            OilDetail NOil = new OilDetail(); //No7
                            NOil.oiltype = "ナフサ";
                            NOil.oiltypeeng = "Naphtha";
                            NOil.oilvol = "5,000L";
                            NOil.capa = "15kL";
                            NOil.oilper = "33.3%";
                            NOil.dlvvol = "2,000L";
                            NOil.salesvol = "500L";
                            odetails.Add(NOil);
                            OilDetail WOil = new OilDetail(); //No8
                            WOil.oiltype = "廃油";
                            WOil.oiltypeeng = "Waste Oil";
                            WOil.oilvol = "2,000L";
                            WOil.capa = "2kL";
                            WOil.oilper = "100.0%";
                            WOil.dlvvol = "0L";
                            WOil.salesvol = "0L";
                            odetails.Add(WOil);
                            OilDetail S2Oil = new OilDetail(); //No9
                            S2Oil.oiltype = "SP-2";
                            S2Oil.oiltypeeng = "SP-2";
                            S2Oil.oilvol = "1,000L";
                            S2Oil.capa = "2kL";
                            S2Oil.oilper = "50.0%";
                            S2Oil.dlvvol = "0L";
                            S2Oil.salesvol = "100L";
                            odetails.Add(S2Oil);
                            OilDetail S4Oil = new OilDetail(); //No10
                            S4Oil.oiltype = "SP-4";
                            S4Oil.oiltypeeng = "SP-4";
                            S4Oil.oilvol = "1,500L";
                            S4Oil.capa = "2kL";
                            S4Oil.oilper = "75.0%";
                            S4Oil.dlvvol = "0L";
                            S4Oil.salesvol = "200L";
                            odetails.Add(S4Oil);
                            OilDetail FOil = new OilDetail(); //No11
                            FOil.oiltype = "FA-1";
                            FOil.oiltypeeng = "FA-1";
                            FOil.oilvol = "2,000L";
                            FOil.capa = "4kL";
                            FOil.oilper = "50.0%";
                            FOil.dlvvol = "1,000L";
                            FOil.salesvol = "300L";
                            odetails.Add(FOil);
                            OilDetail VOil = new OilDetail(); //No11
                            VOil.oiltype = "VT-1";
                            VOil.oiltypeeng = "VT-1";
                            VOil.oilvol = "3,000L";
                            VOil.capa = "4kL";
                            VOil.oilper = "75.0%";
                            VOil.dlvvol = "1,000L";
                            VOil.salesvol = "1,000L";
                            odetails.Add(VOil);
                        }
                        lvdat.odetaillst = odetails;
                        if (zs.bwtrinf == true)
                            lvdat.extrainf = "true";
                        else
                            lvdat.extrainf = "false";
                        jsonstr = JsonSerializer.Serialize(lvdat);
                        return jsonstr;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        if (GlobalVar.testmode == true)
                        {
                            if (skkcode == "M114000006")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                            }
                            else if (skkcode == "M114000007")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                            }
                            else if (skkcode == "M114000008")
                            {
                                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                            }
                        }
                        else
                            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                    }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            else
            {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
            }
            return jsonstr;
        }

        private string GetPerValue(int vol, int capa)
        {
            string perstr= "0%";
            try
            {
                double per = (double)vol / (double)capa;
                perstr = per.ToString("P1");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return perstr;
        }
    }
}
