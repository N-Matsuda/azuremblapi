﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AzureMblApi.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {

        public class SoracomReq
        {
            public string Payload { get; set; }
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values/5
        //public void Post([FromBody]string value)
        //{
        //    Console.WriteLine(value);
        //}
        //public void Post([FromBody]string value)
        public void Post(SoracomReq Req)
        {
            string payload = Req.Payload;
            Console.WriteLine(payload);
        }


        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            Console.WriteLine("get value");
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
