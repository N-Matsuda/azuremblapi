﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Web.Helpers;
using System.Security.Claims;
using System.Security.Cryptography;
using AzureMblApi.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class ReqchgmailController : ApiController
    {
        static string EXISTINGERR = "existing address";
        static string OTHERERR = "other error";
        public class ResultCode
        {
            public string result { get; set; }
            public string cause { get; set; }
        }
        //private readonly RandomNumberGenerator _rng;
        // GET api/Reqchgmail/?email=xxxxx&npasswd=xxxx
        public string Get(string email, string nmail)
        {
            ResultCode rescode = new ResultCode();
            rescode.result = "NG";
            rescode.cause = "None";
            string jsonstr;
            try
            {
                //新パスワードが登録済か確認
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                //DB登録数をカウント
                string sqlstr = "SELECT COUNT(*) FROM AspNetUsers WHERE Email = '" + nmail + "'";
                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                int count = (int)Com.ExecuteScalar();
                if (count > 0)
                {
                    rescode.result = "NG";
                    rescode.cause = EXISTINGERR;
                    cn.Close();
                    jsonstr = JsonSerializer.Serialize(rescode);
                    return jsonstr;
                }
                cn.Close();

                //新パスワード登録
                sqlstr = "UPDATE AspNetUsers SET Email ='" + nmail + "', UserName ='" + nmail + "' WHERE Email = '" + email + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            } catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                rescode.result = "NG";
                rescode.cause = OTHERERR;
                jsonstr = JsonSerializer.Serialize(rescode);
                return jsonstr;
            }
            try
            { 
                //新メールアドレス登録
                string sqlstr = "UPDATE ANUserProfile SET メールアドレス ='" + nmail + "' WHERE メールアドレス = '" + email + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                rescode.result = "OK";
            }
            catch (Exception ex)
            {
                try //AspNetUsersだけemailが更新されていて、ANUserProfileが更新されていない状態を防ぐ
                {
                    string sqlstr = "UPDATE AspNetUsers SET Email ='" + email + "', UserName ='" + email + "' WHERE Email = '" + nmail + "'";
                    DBCtrl.ExecNonQuery(sqlstr);
                }
                catch ( Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
                rescode.result = "NG";
                rescode.cause = OTHERERR;
            }
            jsonstr = JsonSerializer.Serialize(rescode);
            return jsonstr;
        }
    }
}
