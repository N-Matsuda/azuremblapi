﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http;
using System.Text;
using System.Web.Helpers;
using System.Security.Claims;
using System.Security.Cryptography;
using AzureMblApi.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class ReqnewpasswdController : ApiController
    {
        static string PINCODEERR = "pin code error";
        static string EMAILADDRERR = "email address error";
        public class ResultCode
        {
            public string result { get; set; }
            public string cause { get; set; }
        }
        //private readonly RandomNumberGenerator _rng;
        // GET api/Reqnewpasswd/?email=xxxxx&pincode=xxxx&npasswd=xxxx
        public string Get(string email, string pincode, string npasswd)
        {
            ResultCode rescode = new ResultCode();
            rescode.result = "NG";
            rescode.cause = "None";
            string orghash = "";

            string cnstr = GlobalVar.DBCONNECTION;
            string jsonstr;
            try
            {
                //PINコード照合
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr = "SELECT デバイストークン FROM ANUserProfile WHERE メールアドレス='" + email + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                string dbpincode = "0";
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        dbpincode = reader["デバイストークン"] as string;
                        break;
                    }
                }
                cn.Close();
                if( (dbpincode != null) && (dbpincode.Length >=4 ) )
                {
                    try
                    {
                        dbpincode = dbpincode.TrimEnd();
                        //pinコードチェック
                        string pcode = dbpincode.Substring(0, 4);
                        if( pcode != pincode)
                        {
                            rescode.cause = PINCODEERR;
                            jsonstr = JsonSerializer.Serialize(rescode);
                            return jsonstr;
                        }
                        //24時間経過チェック
                        string timestr = dbpincode.Substring(5,16);
                        DateTime dt = DateTime.Parse(timestr);
                        DateTime now = DateTime.Now.ToLocalTime();
                        TimeSpan ts = now - dt;
                        double minpass = ts.TotalMinutes;
                        if(minpass >= 24 * 60)
                        {
                            rescode.cause = PINCODEERR;
                            jsonstr = JsonSerializer.Serialize(rescode);
                            return jsonstr;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        rescode.cause = PINCODEERR;
                        jsonstr = JsonSerializer.Serialize(rescode);
                        return jsonstr;
                    }
                } else
                {
                    rescode.cause = PINCODEERR;
                    jsonstr = JsonSerializer.Serialize(rescode);
                    return jsonstr;
                }

                //password変更
                //SQL参考
                //https://garafu.blogspot.com/2016/05/cs-sqlserver-exec-select.html
                cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                sqlstr = "SELECT PasswordHash FROM AspNetUsers WHERE Email = '" + email + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        orghash = reader["PasswordHash"] as string;
                        break;
                    }
                }
                cn.Close();
                //string hashed = Crypto.HashPassword(password);
                string newhash = PasswordHash.ChangePassword(orghash, npasswd);

                //新パスワード登録
                sqlstr = "UPDATE AspNetUsers SET PasswordHash ='" + newhash + "' WHERE Email = '" + email + "'"; 
                cn.Open();
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                { 
                    while (reader.Read() == true)
                    {
                        orghash = reader["PasswordHash"] as string;
                        break;
                    }
                }
                cn.Close();
                rescode.result = "OK";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                rescode.result = "NG";
            }
            jsonstr = JsonSerializer.Serialize(rescode);
            return jsonstr;
        }
    }
}
