﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Helpers;
using System.Security.Claims;
using System.Security.Cryptography;
using AzureMblApi.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AzureMblApi.Controllers
{
    public class UserauthController : ApiController
    {
        public class AuthResult
        {
            public string result { get; set; }
            public string skkcode { get; set; }
        }
        //private readonly RandomNumberGenerator _rng;
        // GET api/Userauth/?email=xxxxx&passwd=xxxxx
        public string Get(string email, string passwd)
        //public string Get(string passwd)
        {
#if true
            return chkpasswd(email, passwd);
            //return HashPasswordV2(passwd, _rng);
#endif
            //return HashPasswordInOldFormat(passwd);
            //return HashPasswordUsingPBKDF2(passwd);
        }

        private string chkpasswd(string email, string password)
        {
            //calbeeshimotsuma@skkatg.co.jp
            //hash of "Calbeeshimotsu-181130" for test
            //string orghash = "APeJyCo3FEnXOjV8mW9x5VkRd4QsQ+do2p5jtu4eJJpMqzswvWzH9e/dv1HNSYjNIQ==";
            //string pwdchk = new PasswordHasher().HashPassword("Skkkai-202005");
            AuthResult authres = new AuthResult();
            authres.result = "NG";
            authres.skkcode = "None";
            string orghash = "";
            string sqlstr = "SELECT PasswordHash FROM AspNetUsers WHERE Email = '" + email + "'";
            string cnstr = GlobalVar.DBCONNECTION;
            try
            {
                //SQL参考
                //https://garafu.blogspot.com/2016/05/cs-sqlserver-exec-select.html
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                using (var reader = Com.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        orghash = reader["PasswordHash"] as string;
                        break;
                    }
                }
                cn.Close();
                //string hashed = Crypto.HashPassword(password);
                bool res = PasswordHash.VerifyHashedPassword(orghash, password);
                if (res == true)
                {
                    authres.result = "OK";
                    sqlstr = "SELECT SKKコード FROM ANUserProfile WHERE メールアドレス = '" + email + "'";
                    cn.Open();
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    using (var reader = Com.ExecuteReader())
                    {
                        while (reader.Read() == true)
                        {
                            string skkcode = reader["SKKコード"] as string;
                            authres.skkcode = skkcode.TrimEnd();
                            break;
                        }
                    }
                    cn.Close();
                    //ログインステータス登録
                    sqlstr = "UPDATE ANUserProfile SET ログインステータス = '1' WHERE メールアドレス = '" + email + "'";
                    DBCtrl.ExecNonQuery(sqlstr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
            }
            string jsonstr = JsonSerializer.Serialize(authres);
            return jsonstr;
        }

#if false
        //password hash test by referring to url below
        // https://korzh.com/blogs/dotnet-stories/aspnet-identity-migrate-membership-passwords

        public string HashPasswordUsingPBKDF2(string password)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, 16)
            {
                IterationCount = 1000
            };
            byte[] hash = rfc2898DeriveBytes.GetBytes(32);
            byte[] salt = rfc2898DeriveBytes.Salt;
            string hashstr = Convert.ToBase64String(salt) + Convert.ToBase64String(hash);
            return hashstr;
        }
        
        private string HashPasswordInOldFormat(string password)
        {
		    var sha1 = new SHA1CryptoServiceProvider();
            var data = Encoding.ASCII.GetBytes(password);
            var sha1data = sha1.ComputeHash(data);
		    string hash = Convert.ToBase64String(sha1data);
            return hash;
	    }

        //Password hash test by referring to url below.
        // https://andrewlock.net/exploring-the-asp-net-core-identity-passwordhasher/?search=RandomNumberGenerator
        private static string HashPasswordV2(string password, RandomNumberGenerator rng)
        {
            const KeyDerivationPrf Pbkdf2Prf = KeyDerivationPrf.HMACSHA1; // default for Rfc2898DeriveBytes
            const int Pbkdf2IterCount = 1000; // default for Rfc2898DeriveBytes
            const int Pbkdf2SubkeyLength = 256 / 8; // 256 bits
            const int SaltSize = 128 / 8; // 128 bits

            // Produce a version 2 text hash.
            byte[] salt = new byte[SaltSize];
            rng.GetBytes(salt);
            byte[] subkey = KeyDerivation.Pbkdf2(password, salt, Pbkdf2Prf, Pbkdf2IterCount, Pbkdf2SubkeyLength);

            var outputBytes = new byte[1 + SaltSize + Pbkdf2SubkeyLength];
            outputBytes[0] = 0x00; // format marker
            Buffer.BlockCopy(salt, 0, outputBytes, 1, SaltSize);
            Buffer.BlockCopy(subkey, 0, outputBytes, 1 + SaltSize, Pbkdf2SubkeyLength);
            return Convert.ToBase64String(outputBytes);
        }
#endif

    }
}
