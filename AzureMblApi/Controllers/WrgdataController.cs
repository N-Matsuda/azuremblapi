﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using AzureMblApi.Models;

namespace AzureMblApi.Controllers
{
    public class WrgdataController : ApiController
    {
        //HttpGet api/Wrgdata/?skkcode="SKKCODE"&min="
        public string Get(string skkcode, string min)
        {
            string data = "";
            string jsonstr = "[{\"skkcode\"; null, \"ssname\":null, \"datetime\":null, \"tankno\":null, \"wrgtype\":null,\"wrgtypeeng\":null,\"onoff\":null}]";
            try
            {
                if (GlobalVar.testmode == true)
                {
                    if (skkcode == "M114000009")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                    }
                    else if (skkcode == "M114000010")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                    }
                    else if (skkcode == "M114000011")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                    }
                }
                List<wrgdata> wrglist = new List<wrgdata>();
                DateTime dt = DateTime.Now;
                int imin = 0;

                if ( true == int.TryParse(min, out imin) )
                {
                    double dmin = (double)(-1 * imin);
                    dt = dt.AddMinutes(-imin);
                }
                SiraPtlWrgHis wrghis = new SiraPtlWrgHis();
                wrghis.OpenTableBySkkcodeAndDate(skkcode, dt);
                int num = wrghis.GetNumOfRecord();
                if (num > 0)
                {
                    for (int i = 0; i < num; i++)
                    {
                        wrgdata newss = wrghis.GetWrgDatByLine(i);
                        if( newss != null )
                            wrglist.Add(newss);
                    }
                    jsonstr = JsonSerializer.Serialize(wrglist);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (GlobalVar.testmode == true)
                {
                    if (skkcode == "M114000009")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Ambiguous));
                    }
                    else if (skkcode == "M114000010")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));

                    }
                    else if (skkcode == "M114000011")
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadGateway));
                    }
                    else
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                    }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return jsonstr;
        }
    }
}
