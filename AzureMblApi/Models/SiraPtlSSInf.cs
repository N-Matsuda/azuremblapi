﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace AzureMblApi.Models
{
    public class SiraPtlSSInf
    {
        private DataTable SPSSInfDT;

        private const int SiteNameColNo = 0;    //施設名
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int CompNameColNo = 2;    //会社名
        private const int LowStatusColNo = 3;   //減状態
        private const int HighStatusColNo = 4;  //満状態
        private const int WtrStatusColNo = 5;   //水状態
        private const int SnsStatusColNo = 6;   //センサー異常状態
        private const int LC4StatusColNo = 7;   //漏えい点検状態
        private const int LkStatusColNo = 8;  //リーク状態
        private const int NoDataColNo = 9;      //データ更新無し状態
        private const int UpdateDateColNo = 10;  //データ更新日時
        private string nowrgstr = "00000000000000000000";

        //Constructer
        public SiraPtlSSInf()
        {
            DataTableCtrl.InitializeTable(SPSSInfDT);
        }

        //テーブル読み込み
        public void OpenTable(string companyname)
        {
            try
            {
                string sqlstr;
                if (companyname == "*")
                    sqlstr = "SELECT * FROM SiraPtlSSInf";
                else
                    sqlstr = "SELECT * FROM SiraPtlSSInf WHERE 会社名= (N'" + companyname + "')";
                DataTableCtrl.InitializeTable(SPSSInfDT);
                SPSSInfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPSSInfDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //テーブル読み込み
        public void OpenTableSkkcode(string skkcode, string wrg, string ssname)
        {
            try
            {
                string sqlstr = "";
                if (skkcode.Length >= 10)
                {
                    sqlstr = "SELECT * SiraPtlSSInf WHERE SKKコード= '" + skkcode.Substring(0, 10) + "'";
                }
                else
                {
                    sqlstr = "SELECT * FROM SiraPtlSSInf WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                if( ssname != "*")
                {
                    sqlstr += " AND 施設名 LIKE N'%" + ssname + "%'";
                }
                if( wrg != "*")
                {
                    if (wrg == "0") //減警報
                    {
                        sqlstr += " AND 減状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "1") //満警報
                    {
                        sqlstr += " AND 満状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "2") //水警報
                    {
                        sqlstr += " AND 水状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "3") //センサー異常
                    {
                        sqlstr += " AND センサー異常状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "4") //漏えい点検
                    {
                        sqlstr += " AND 漏えい点検状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "5") //リーク
                    {
                        sqlstr += " AND リーク状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "6") //データ無し
                    {
                        sqlstr += " AND データ更新無し状態 <> '00000000000000000000'";
                    }
                    else if (wrg == "99") //全ての警報
                    {
                        sqlstr += " AND 減状態 <> '00000000000000000000' OR 満状態 <> '00000000000000000000' OR 水状態 <> '00000000000000000000' OR センサー異常状態 <> '00000000000000000000' OR 漏えい点検状態 <> '00000000000000000000' OR リーク状態 <> '00000000000000000000' OR データ更新無し状態 <> '00000000000000000000'";
                    }
                }
                DataTableCtrl.InitializeTable(SPSSInfDT);
                SPSSInfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPSSInfDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            if (SPSSInfDT != null)
                return SPSSInfDT.Rows.Count;
            return 0;
        }

        //SKKコードより施設名を取得
        public string GetSiteName(string skkcode)
        {
            string sitename = "";
            try
            {
                int num = SPSSInfDT.Rows.Count;
                for (int i = 0; i < num; i++)
                {
                    if (skkcode == SPSSInfDT.Rows[i][SKKCodeColNo].ToString().TrimEnd())
                        sitename = SPSSInfDT.Rows[i][SiteNameColNo].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return sitename;
        }
        //指定行、SKKコードを取り出す
        public string GetSkkcodeByLine(int lineno)
        {
            string skkcode = "";
            try
            {
                skkcode = SPSSInfDT.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return skkcode;
        }

        //指定行、SKK名を取り出す
        public string GetSiteNameByLine(int lineno)
        {
            string ssname = "";
            try
            {
                ssname = SPSSInfDT.Rows[lineno][SiteNameColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return ssname;
        }
        //指定行,発生中の警報を取り出す
        public (string wrg, string wrgeng) GetSiteWrgTypeByLine(int lineno)
        {
            string wrgstr = "";
            string wrgeng = "";
            try
            {
                string wrgs = SPSSInfDT.Rows[lineno][LowStatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //減警報チェック
                {
                    wrgstr += "減,";
                    wrgeng += "Low,";
                }
                wrgs = SPSSInfDT.Rows[lineno][HighStatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //満警報チェック
                {
                    wrgstr += "満,";
                    wrgeng += "Full,";
                }
                wrgs = SPSSInfDT.Rows[lineno][WtrStatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //水警報チェック
                {
                    wrgstr += "水検知,";
                    wrgeng += "Water,";
                }
                wrgs = SPSSInfDT.Rows[lineno][SnsStatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //センサー異常状態チェック
                {
                    wrgstr += "センサ異常,";
                    wrgeng += "Sensor,";
                }
                wrgs = SPSSInfDT.Rows[lineno][LC4StatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //漏えい点検状態
                {
                    wrgstr += "漏えい,";
                    wrgeng += "Leak test,";
                }
                wrgs = SPSSInfDT.Rows[lineno][LkStatusColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //リーク状態
                {
                    wrgstr += "リーク,";
                    wrgeng += "Leak,";
                }
                wrgs = SPSSInfDT.Rows[lineno][NoDataColNo].ToString().TrimEnd();
                if (wrgs != nowrgstr) //データ更新無し状態
                {
                    wrgstr += "データ更新異常,";
                    wrgeng += "No data,";
                }
                if (wrgstr.Length > 1)
                {
                    wrgstr = wrgstr.Substring(0, wrgstr.Length - 1); //最後の","をとる
                    wrgeng = wrgeng.Substring(0, wrgeng.Length - 1); //最後の","をとる
                }
                if (wrgstr == "")
                {
                    wrgstr = "通常";
                    wrgeng = "no error";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return (wrgstr, wrgeng);
        }
               
        //指定行、指定タンクの減状態を取り出す
        public bool ChkLowWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string lowsts = SPSSInfDT.Rows[lineno][LowStatusColNo].ToString().TrimEnd();
                lowsts = lowsts.Substring(tno, 1);
                if (lowsts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの減状態を変更する
        public bool ChgLowWrgSts(int lineno, int tno, bool newlowsts, string skkcode)
        {
            bool bret = false;
            try
            {
                string lowsts = SPSSInfDT.Rows[lineno][LowStatusColNo].ToString().TrimEnd();
                if (newlowsts == true)
                    lowsts = lowsts.Remove(tno, 1).Insert(tno, "1");
                else
                    lowsts = lowsts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][LowStatusColNo] = lowsts;

                string sqlstr = "UPDATE SiraPtlSSInf SET 減状態= '" + lowsts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //指定行、指定タンクの満状態を取り出す
        public bool ChkHighWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string hists = SPSSInfDT.Rows[lineno][HighStatusColNo].ToString().TrimEnd();
                hists = hists.Substring(tno, 1);
                if (hists == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの満状態を変更する
        public bool ChgHighWrgSts(int lineno, int tno, bool newhists, string skkcode)
        {
            bool bret = false;
            try
            {
                string hists = SPSSInfDT.Rows[lineno][HighStatusColNo].ToString().TrimEnd();
                if (newhists == true)
                    hists = hists.Remove(tno, 1).Insert(tno, "1");
                else
                    hists = hists.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][HighStatusColNo] = hists;

                string sqlstr = "UPDATE SiraPtlSSInf SET 満状態= '" + hists + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの水警報状態を取り出す
        public bool ChkWtrWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string wtsts = SPSSInfDT.Rows[lineno][WtrStatusColNo].ToString().TrimEnd();
                wtsts = wtsts.Substring(tno, 1);
                if (wtsts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの水警報状態を変更する
        public bool ChgWtrWrgSts(int lineno, int tno, bool newtsts, string skkcode)
        {
            bool bret = false;
            try
            {
                string wtsts = SPSSInfDT.Rows[lineno][WtrStatusColNo].ToString().TrimEnd();
                if (newtsts == true)
                    wtsts = wtsts.Remove(tno, 1).Insert(tno, "1");
                else
                    wtsts = wtsts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][WtrStatusColNo] = wtsts;

                string sqlstr = "UPDATE SiraPtlSSInf SET 水状態= '" + wtsts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクのセンサー異常警報状態を取り出す
        public bool ChkSnsWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string snssts = SPSSInfDT.Rows[lineno][SnsStatusColNo].ToString().TrimEnd();
                snssts = snssts.Substring(tno, 1);
                if (snssts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクのセンサー異常警報状態を変更する
        public bool ChgSnsWrgSts(int lineno, int tno, bool newsnssts, string skkcode)
        {
            bool bret = false;
            try
            {
                string snssts = SPSSInfDT.Rows[lineno][SnsStatusColNo].ToString().TrimEnd();
                if (newsnssts == true)
                    snssts = snssts.Remove(tno, 1).Insert(tno, "1");
                else
                    snssts = snssts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][SnsStatusColNo] = snssts;

                string sqlstr = "UPDATE SiraPtlSSInf SET センサー異常状態= '" + snssts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの漏えい点検警報警報状態を取り出す
        public bool ChkLc4WrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string lc4sts = SPSSInfDT.Rows[lineno][LC4StatusColNo].ToString().TrimEnd();
                lc4sts = lc4sts.Substring(tno, 1);
                if (lc4sts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクの漏えい点検警報状態を変更する
        public bool ChgLc4WrgSts(int lineno, int tno, bool newlcsts, string skkcode)
        {
            bool bret = false;
            try
            {
                string lc4sts = SPSSInfDT.Rows[lineno][LC4StatusColNo].ToString().TrimEnd();
                if (newlcsts == true)
                    lc4sts = lc4sts.Remove(tno, 1).Insert(tno, "1");
                else
                    lc4sts = lc4sts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][LC4StatusColNo] = lc4sts;

                string sqlstr = "UPDATE SiraPtlSSInf SET 漏えい点検状態= '" + lc4sts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクのデータ無し警報状態を取り出す
        public bool ChkNoDataWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string ndsts = SPSSInfDT.Rows[lineno][NoDataColNo].ToString().TrimEnd();
                ndsts = ndsts.Substring(tno, 1);
                if (ndsts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクのデータ無し警報状態を変更する
        public bool ChgNoDataWrgSts(int lineno, int tno, bool newndsts, string skkcode)
        {
            bool bret = false;
            try
            {
                string ndsts = SPSSInfDT.Rows[lineno][NoDataColNo].ToString().TrimEnd();
                if (newndsts == true)
                    ndsts = ndsts.Remove(tno, 1).Insert(tno, "1");
                else
                    ndsts = ndsts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][NoDataColNo] = ndsts;

                string sqlstr = "UPDATE SiraPtlSSInf SET データ更新無し状態= '" + ndsts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //指定行、指定タンクのリーク警報警報状態を取り出す
        public bool ChkLkWrgSts(int lineno, int tno)
        {
            bool bret = false;
            try
            {
                string lksts = SPSSInfDT.Rows[lineno][LkStatusColNo].ToString().TrimEnd();
                lksts = lksts.Substring(tno, 1);
                if (lksts == "1")    //現状帯
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定行、指定タンクのリーク警報状態を変更する
        public bool ChgLkWrgSts(int lineno, int tno, bool newlksts, string skkcode)
        {
            bool bret = false;
            try
            {
                string lksts = SPSSInfDT.Rows[lineno][LkStatusColNo].ToString().TrimEnd();
                if (newlksts == true)
                    lksts = lksts.Remove(tno, 1).Insert(tno, "1");
                else
                    lksts = lksts.Remove(tno, 1).Insert(tno, "0");
                SPSSInfDT.Rows[lineno][LkStatusColNo] = lksts;

                string sqlstr = "UPDATE SiraPtlSSInf SET リーク状態= '" + lksts + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //データ更新日時を指定する。
        public bool ChgTimeStamp(int lineno, DateTime dt, string skkcode)
        {
            bool bret = false;
            try
            {
                string datestr = dt.ToString("yyyyMMddHHmm");
                SPSSInfDT.Rows[lineno][UpdateDateColNo] = datestr;

                string sqlstr = "UPDATE SiraPtlSSInf SET データ更新日時= '" + datestr + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

    }
}