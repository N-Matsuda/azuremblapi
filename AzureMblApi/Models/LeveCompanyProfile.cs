﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace AzureMblApi.Models
{
    public class LeveCompanyProfile
    {
        DataTable LVCompProfDT;
        private const int CompNoCollNo = 0;         //会社番号
        private const int CompName1ColNo = 1;       //会社名
        private const int CompName2ColNo = 2;       //会社名2
        private const int MRDefCodeColNo = 3;       //モバイルルーター初期コード
        private const int GWDefCodeColNo = 4;       //ゲートウェイ初期コード
        private const int DefSKKCodeColNo = 5;      //デフォルトSKKコード
        private const int DefSSNameColNo = 6;       //初期SS名
        private const int AdminNameColNo = 7;       //管理者名
        private const int SubAdminNameColNo = 8;    //副管理者名
        private const int MaxTankNoColNo = 9;       //最大タンク数
        private const int MailAdrColNo = 10;        //メールアドレス1
        public int compno;              //会社番号
        public string compname1;        //会社名
        public string compname2;        //会社名2
        public string mrdefcode;        //モバイルルーター初期コード
        public string gwdefcode;        //ゲートウェイ初期コード
        public string defskkcode;       //デフォルトSKKコード
        public string defssname;        //初期SS名
        public string adminname;        //管理者名
        public string subadminname;     //副管理者名
        public int maxtankno;           //最大タンク数
        public string mailadr1;         //メールアドレス1

        //コンストラクター
        public LeveCompanyProfile()
        {
            DataTableCtrl.InitializeTable(LVCompProfDT);
        }

        //ASP.NETユーザーテーブル読み込み
        public void OpenTable(string compname)
        {
            try
            {
                string sqlstr = "SELECT * FROM LeveCompanyProfile WHERE 会社名 = '" + compname + "'";
                DataTableCtrl.InitializeTable(LVCompProfDT);
                LVCompProfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, LVCompProfDT);
                GetProperty();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //ASP.NETユーザーテーブル読み込み　会社番号
        public void OpenTableById(int companyno)
        {
            try
            {
                string sqlstr = "SELECT * FROM LeveCompanyProfile WHERE 会社番号 = '" + companyno.ToString() + "'";
                DataTableCtrl.InitializeTable(LVCompProfDT);
                LVCompProfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, LVCompProfDT);
                GetProperty();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void GetProperty()
        {
            try
            {
                if ((LVCompProfDT != null) && (LVCompProfDT.Rows.Count > 0))
                {
                    compno = (int)LVCompProfDT.Rows[0][CompNoCollNo];
                    compname1 = LVCompProfDT.Rows[0][CompName1ColNo].ToString().TrimEnd();
                    compname2 = LVCompProfDT.Rows[0][CompName2ColNo].ToString().TrimEnd();
                    mrdefcode = LVCompProfDT.Rows[0][MRDefCodeColNo].ToString().TrimEnd();
                    gwdefcode = LVCompProfDT.Rows[0][GWDefCodeColNo].ToString().TrimEnd();
                    defskkcode = LVCompProfDT.Rows[0][DefSKKCodeColNo].ToString().TrimEnd();
                    defssname = LVCompProfDT.Rows[0][DefSSNameColNo].ToString().TrimEnd();
                    adminname = LVCompProfDT.Rows[0][AdminNameColNo].ToString().TrimEnd();
                    subadminname = LVCompProfDT.Rows[0][SubAdminNameColNo].ToString().TrimEnd();
                    maxtankno = (int)LVCompProfDT.Rows[0][MaxTankNoColNo];
                    mailadr1 = LVCompProfDT.Rows[0][MailAdrColNo].ToString().TrimEnd();
                }
                else
                {
                    SetDefValue();
                }
            }
            catch (Exception e)
            {
                SetDefValue();
                Debug.WriteLine(e.ToString());
            }
        }

        private void SetDefValue()
        {
            compno = (int)GlobalVar.CompanySKKNo;
            compname1 = GlobalVar.SKKCompanyName;
            compname2 = GlobalVar.SKKCompanyName;
            mrdefcode = GlobalVar.SKKCompMBUSkkcode;
            gwdefcode = GlobalVar.SKKCompGWUSkkcode;
            defskkcode = GlobalVar.SKKDefSkkcode1;
            adminname = GlobalVar.SKKAdminName;
            subadminname = GlobalVar.SKKSubAdminName;
            maxtankno = GlobalVar.SkkMaxTankNo;
            mailadr1 = GlobalVar.SKKMlAdr1;
        }
    }
}