﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace AzureMblApi.Models
{
    public class SiteInf
    {
        private DataTable SiteInfDT;
        private DataTable TankCapaDT;
        private DataTable LowVolumeDT;
        private DataTable OilTypeDT;

        private DataTable T1To10Table;
        private DataTable T11To20Table;

        private int numtank;
        //コンストラクタ
        public SiteInf()
        {
            DataTableCtrl.InitializeTable(SiteInfDT);
        }

        //テーブル読み込み SKKコード
        public int OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM SiteInf WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SiteInfDT);
                SiteInfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SiteInfDT);

                int i;
                string otype;
                for (i = 0; i < GlobalVar.NUMTANK; i++)
                {
                    try
                    {
                        otype = (string)SiteInfDT.Rows[0][2 + 4 * i];
                    }
                    catch (Exception ex)
                    {
                        break;
                    }
                }
                numtank = i;
                return numtank;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        //有効なタンクの数を返す
        public int GetNumTank()
        {
            return numtank;
        }

        //指定液種のタンク番号配列を返す
        public int[] GetTankArrFromOilType(string oltype)
        {
            List<int> tnklist = new List<int>();
            int val;
            for (int i = 0; i < numtank; i++)
            {
                if (SiteInfDT.Rows[0][2 + 4 * i] == null)
                    break;
                if (SiteInfDT.Rows[0][2 + 4 * i].ToString().TrimEnd() == oltype)
                    tnklist.Add(i + 1);
            }
            int[] tnkarr = tnklist.ToArray();
            return tnkarr;
        }

        //各タンクの液種配列を返答
        public string[] GetOilType()
        {
            string[] oilarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                if (SiteInfDT.Rows[0][2 + 4 * i] == null)
                    break;
                oilarr[i] = SiteInfDT.Rows[0][2 + 4 * i].ToString().TrimEnd();
            }
            return oilarr;
        }
        //指定タンクの液種
        public string GetOilTypeByTank(int tankno)
        {
            string oiltype = "";
            try
            {
                oiltype = SiteInfDT.Rows[0][2 + 4 * (tankno - 1)].ToString().TrimEnd();
            }
            catch (Exception ex)
            {
                ;
            }
            return oiltype;
        }

        //各タンクの連結情報を返答
        public string[] GetLinkInf()
        {
            string[] linkarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 1] == null)
                        break;
                    linkarr[i] = SiteInfDT.Rows[0][2 + 4 * i + 1].ToString();
                }
                catch (Exception ex)
                {
                    break;
                }

            }
            return linkarr;
        }

        //連結2番目以降のタンクをはぶくタンク数列
        public string[] GetTankNoString()
        {
            string lnkinf;
            List<string> tnklist = new List<string>();

            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    lnkinf = GetLinkInfByTank(i + 1);
                    if (lnkinf.IndexOf("連結なし") >= 0)
                        tnklist.Add("タンク" + (i + 1).ToString());
                    else if (lnkinf.IndexOf("連結") >= 0)
                    {
                        lnkinf = lnkinf.Substring(6);
                        if (lnkinf.StartsWith((i + 1).ToString()) == false)
                            tnklist.Add("タンク" + (i + 1).ToString());
                    }
                    else
                        tnklist.Add("タンク" + (i + 1).ToString());

                }
                catch (Exception ex)
                {
                    break;
                }

            }
            return tnklist.ToArray();
        }

        //指定タンクの連結情報
        public string GetLinkInfByTank(int tankno)
        {
            try
            {
                string linkinf = SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 1].ToString();
                string linkstr = "";
                byte[] bytearr = System.Text.Encoding.GetEncoding("ASCII").GetBytes(linkinf);
                byte blnk;
                for (int i = 0; i < 8; i++)
                {
                    blnk = bytearr[i];
                    if (blnk >= 0x60)
                        linkstr = "タンク20 ";
                    else if (blnk >= 0x50)
                        linkstr = "タンク1" + (blnk & 0xf).ToString() + " ";
                    else if (blnk > 0x40)
                        linkstr = "タンク" + (blnk & 0xf).ToString() + " ";
                }
                if (linkstr == "")
                    return "連結なし";
                else
                    return "連結:" + linkstr;
            }
            catch (Exception ex)
            {
                return "連結なし";
            }
        }

        //各タンクの容量
        public string[] GetCapacity()
        {
            string[] capaarr = new string[numtank];
            int vol;
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 2] == null)
                        break;
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 2];
                    capaarr[i] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            return capaarr;
        }

        //指定液種の容量
        public int GetCapacityByOil(string oltype)
        {

            int capa = 0;
            for (int i = 0; i < numtank; i++)
            {
                if (SiteInfDT.Rows[0][2 + 4 * i] == null)
                    break;
                if (SiteInfDT.Rows[0][2 + 4 * i].ToString().TrimEnd() == oltype)
                    capa += (int)SiteInfDT.Rows[0][2 + 4 * i + 2];
            }
            return capa;
        }


        //指定タンクの容量
        public int GetCapacityByTank(int tankno)
        {
            int capa = 0;
            try
            {
                capa = (int)SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 2];
            }
            catch (Exception ex)
            {
                ;
            }
            return capa;
        }

        //各タンクの減警報値
        public string[] GetLowWarning()
        {
            string[] lowarr = new string[numtank];
            int vol;
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 3] == null)
                        break;
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 3];
                    lowarr[i] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    ;
                }
            }
            return lowarr;
        }

        //指定タンクの下限警報値取得
        public int GetLowWarningByTank(int tankno)
        {
            int capa = 0;
            try
            {
                capa = (int)SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 3];
            }
            catch (Exception ex)
            {
                ;
            }
            return capa;
        }

        //指定タンクの下限警報値登録
        public bool SetLowWarningbyTank(int tankno, string lvolst)
        {
            bool res = true;
            int lvol, capa;
            try
            {
                lvolst = lvolst.Replace(",", "");
                lvol = int.Parse(lvolst);
                try
                {
                    capa = (int)SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 2];
                }
                catch (Exception ex)
                {
                    capa = 0;
                }
                if (lvol > capa)
                    return false;
                SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 3] = lvolst;
            }
            catch (Exception ex)
            {
                res = false;
            }
            return res;
        }

        //下限警報値のDB登録
        public bool UpdateLowWarningValue(string skkcode)
        {
            bool res = true;
            try
            {
                string sqlstr = "UPDATE SiteInf SET " +
                         "タンク1減警報値= '" + SiteInfDT.Rows[0][2 + 3].ToString() +
                         "', タンク2減警報値= '" + SiteInfDT.Rows[0][2 + 4 + 3].ToString() +
                         "', タンク3減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 2 + 3].ToString() +
                         "', タンク4減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 3 + 3].ToString() +
                         "', タンク5減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 4 + 3].ToString() +
                         "', タンク6減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 5 + 3].ToString() +
                         "', タンク7減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 6 + 3].ToString() +
                         "', タンク8減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 7 + 3].ToString() +
                         "', タンク9減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 8 + 3].ToString() +
                         "', タンク10減警報値= '" + SiteInfDT.Rows[0][2 + 4 * 9 + 3].ToString() +
                         "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                res = false;
            }
            return res;
        }

        //タンク容量値テーブル作成
        public DataTable GetCapaTable()
        {
            int vol;
            DataTableCtrl.InitializeTable(TankCapaDT);
            TankCapaDT = new DataTable();
            string[] columnlst = { "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            TankCapaDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
            DataRow dEditRow = TankCapaDT.NewRow();
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 2];
                    dEditRow[i] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    dEditRow[i] = 0;
                }
            }
            TankCapaDT.Rows.Add(dEditRow);
            return TankCapaDT;
        }

        //油種テーブル作成
        public DataTable GetOilTypeTable()
        {
            DataTableCtrl.InitializeTable(OilTypeDT);
            OilTypeDT = new DataTable();
            string[] columnlst = { "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            OilTypeDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
            DataRow dEditRow = OilTypeDT.NewRow();
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    dEditRow[i] = SiteInfDT.Rows[0][2 + 4 * i].ToString();
                }
                catch (Exception ex)
                {
                    dEditRow[i] = 0;
                }
            }
            OilTypeDT.Rows.Add(dEditRow);
            return OilTypeDT;
        }

        //タンク1-10のテーブル作成
        public DataTable GetTank1To10Table()
        {
            int vol;
            T1To10Table = new DataTable();
            string[] columnlst = { "Name", "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            T1To10Table.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            DataRow dEditRow = T1To10Table.NewRow();
            dEditRow[0] = "液種";
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    string oltype = SiteInfDT.Rows[0][2 + 4 * i].ToString();
                    if (string.IsNullOrWhiteSpace(oltype) == true)
                        oltype = "無し";
                    dEditRow[i + 1] = oltype;
                }
                catch (Exception ex)
                {
                    dEditRow[i + 1] = " ";
                }
            }
            T1To10Table.Rows.Add(dEditRow);

            dEditRow = T1To10Table.NewRow();
            dEditRow[0] = "申請容量(L)";
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 2];
                    dEditRow[i + 1] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    dEditRow[i + 1] = 0;
                }
            }
            T1To10Table.Rows.Add(dEditRow);
            return T1To10Table;
        }

        //タンク11-20のテーブル作成
        public DataTable GetTank11To20Table()
        {
            int vol;
            T11To20Table = new DataTable();
            string[] columnlst = { "Name", "ﾀﾝｸ11", "ﾀﾝｸ12", "ﾀﾝｸ13", "ﾀﾝｸ14", "ﾀﾝｸ15", "ﾀﾝｸ16", "ﾀﾝｸ17", "ﾀﾝｸ18", "ﾀﾝｸ19", "ﾀﾝｸ20" };
            T11To20Table.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            DataRow dEditRow = T11To20Table.NewRow();
            dEditRow[0] = "液種";
            for (int i = 10; i < 20; i++)
            {
                try
                {
                    string oltype = SiteInfDT.Rows[0][2 + 4 * i].ToString();
                    if (string.IsNullOrWhiteSpace(oltype) == true)
                        oltype = "無し";
                    dEditRow[i - 10 + 1] = oltype;
                }
                catch (Exception ex)
                {
                    dEditRow[i - 10 + 1] = " ";
                }
            }

            T11To20Table.Rows.Add(dEditRow);
            dEditRow[0] = "申請容量(L)";
            dEditRow = T11To20Table.NewRow();
            for (int i = 10; i < 20; i++)
            {
                try
                {
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 2];
                    dEditRow[i - 10 + 1] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    dEditRow[i - 10 + 1] = 0;
                }
            }
            T11To20Table.Rows.Add(dEditRow);
            return T11To20Table;
        }

        //下限警報値テーブル作成
        public DataTable GetLWrgTable()
        {
            int vol;
            DataTableCtrl.InitializeTable(LowVolumeDT);
            LowVolumeDT = new DataTable();
            string[] columnlst = { "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            LowVolumeDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
            DataRow dEditRow = LowVolumeDT.NewRow();
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    vol = (int)SiteInfDT.Rows[0][2 + 4 * i + 3];
                    dEditRow[i] = vol.ToString("#,0");
                }
                catch (Exception ex)
                {
                    dEditRow[i] = 0;
                }
            }
            LowVolumeDT.Rows.Add(dEditRow);
            return LowVolumeDT;
        }
    }
}