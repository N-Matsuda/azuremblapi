﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace AzureMblApi.Models
{
    public class SiraPtlUserProfile
    {
        private DataTable SPUserPrfDT;
        private DataTable CheckTableDT;
        private List<string> CheckedSSList;
        private const string stDisp = "表示";
        private const string stSkkcode = "SKKコード";
        private const string stSiteName = "施設名";

        private const int UserNameColNo = 0;    //ユーザー名
        private const int MailAdrColNo = 1;     //メールアドレス
        private const int CompNameColNo = 2;    //会社名
        private const int CompNoColNo = 3;      //会社番号
        private const int UserTypeColNo = 4;    //ユーザータイプ 1:地域管理者
        private const int SiraDispColNo = 5;    //1:SIRA画面表示
        private const int LeveDispColNo = 6;    //1:Leve画面表示
        private const int LowMailColNo = 7;     //1:減メール送信
        private const int HighMailColNo = 8;    //1:満メール送信
        private const int WtrMailColNo = 9;     //1:水メール送信
        private const int SnsMailColNo = 10;     //1:センサー異常メール送信
        private const int LC4MailColNo = 11;    //1:漏えい点検メール送信
        private const int LeakMailColNo = 12;   //1:リークメール送信
        private const int NoDataMailColNo = 13; //1:データ更新無しメール送信
        private const int DispSSColNO = 14;     //表示SSコード

        //Constructer
        public SiraPtlUserProfile()
        {
            DataTableCtrl.InitializeTable(SPUserPrfDT);
        }

        //テーブル読み込み
        public void OpenTableByCompany(string companyname)
        {
            try
            {
                string sqlstr;
                if (companyname == "*")
                    sqlstr = "SELECT * FROM SiraPtlUserProfile";
                else
                    sqlstr = "SELECT * FROM SiraPtlUserProfile WHERE 会社名= (N'" + companyname + "')";
                DataTableCtrl.InitializeTable(SPUserPrfDT);
                SPUserPrfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPUserPrfDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public void OpenTableCompanyNo(int companyno)
        {
            try
            {
                string sqlstr = "SELECT * FROM SiraPtlUserProfile WHERE 会社番号= '" + companyno.ToString() + "'";
                DataTableCtrl.InitializeTable(SPUserPrfDT);
                SPUserPrfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPUserPrfDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたユーザー名に対するテーブル読み込み
        public void OpenTableByUser(string username)
        {
            try
            {
                string sqlstr;
                if (username == "*")
                    sqlstr = "SELECT * FROM SiraPtlUserProfile";
                else
                    sqlstr = "SELECT * FROM SiraPtlUserProfile WHERE ユーザー名= '" + username + "'";
                DataTableCtrl.InitializeTable(SPUserPrfDT);
                SPUserPrfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPUserPrfDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定ユーザー削除
        public static void DeleteUser(string username)
        {
            try
            {
                string sqlstr = "DELETE FROM SiraPtlUserProfile WHERE ユーザー名= '" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //ユーザー権限変更->地域管理者へ
        public static bool UpUserPrv(string username)
        {
            bool bret = false;
            try
            {
                string sqlstr = "UPDATE SiraPtlUserProfile SET ユーザータイプ='1' WHERE ユーザー名= '" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }
        //ユーザー権限変更->一般へ
        public static bool DownUserPrv(string username)
        {
            bool bret = false;
            try
            {
                string sqlstr = "UPDATE SiraPtlUserProfile SET ユーザータイプ='0' WHERE ユーザー名= '" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                bret = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }
#if false
        //ユーザー追加
        public static void AddUser(string username, string mailaddr, int companyno)
        {
            try
            {
                //skkcode配列を求める
                LeveCompanyProfile lvcprof = new LeveCompanyProfile();
                lvcprof.OpenTableById(companyno);

                DataUnitTable dttble = new DataUnitTable();
                dttble.OpenTableSkkcodesGwMbl(lvcprof.gwdefcode, lvcprof.mrdefcode, "*", lvcprof.compname2);
                List<string> scodelst = dttble.GetSkkcodeList();
                string slst = "";
                bool bfirst = true;
                foreach (string scode in scodelst)
                {
                    if (bfirst == true)
                    {
                        bfirst = false;
                    }
                    else
                    {
                        slst += ",";    //2番目以降 ,A00xxxxxx
                    }
                    slst += scode;
                }

                //指定ユーザーメールアドレス追加
                string sqlstr = "INSERT INTO SiraPtlUserProfile ( ユーザー名, メールアドレス, 会社名, ユーザータイプ, SIRA画面表示, LEVE画面表示, 減メール, 満メール, 水メール, センサー異常メール, 漏えい点検メール, リークメール, データ更新無しメール, 表示SS1 ) VALUES "
                    + "('" + username + "','" + mailaddr + "','" + "N'" + lvcprof.compname2 + "','0','1','1','0','0','0','0','0','0','0','" + slst + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
#endif
        //ユーザー名リスト
        public List<string> GetUserList()
        {
            List<string> ulist = new List<string>();
            try
            {
                for (int i = 0; i < SPUserPrfDT.Rows.Count; i++)
                {
                    ulist.Add(SPUserPrfDT.Rows[i][UserNameColNo].ToString().TrimEnd());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ulist;

        }

        //ユーザー名リスト(権限つき)
        public List<string> GetUserListWithPriv()
        {
            List<string> ulist = new List<string>();
            try
            {
                for (int i = 0; i < SPUserPrfDT.Rows.Count; i++)
                {
                    string uname = SPUserPrfDT.Rows[i][UserNameColNo].ToString().TrimEnd();
                    if ((int)SPUserPrfDT.Rows[i][UserTypeColNo] > 0)
                        ulist.Add("(地域管理者)" + uname);
                    else
                        ulist.Add(uname);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ulist;

        }
        //ユーザー名権限リスト
        public List<string> GetUserPrivList()
        {
            List<string> ulist = new List<string>();
            try
            {
                for (int i = 0; i < SPUserPrfDT.Rows.Count; i++)
                {
                    string uname = SPUserPrfDT.Rows[i][UserNameColNo].ToString().TrimEnd();
                    if ((int)SPUserPrfDT.Rows[i][UserTypeColNo] > 0)
                        ulist.Add("地域管理者");
                    else
                        ulist.Add("一般");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ulist;

        }

        //ユーザー情報更新
        public static void WriteWrgHistory(string username, bool bshowsira, bool bshowleve, bool bsendlow, bool bsendhi, bool bsendwtr, bool bsendsns, bool bsendlc4, bool bsendlk, bool bsendnodt)
        {
            try
            {
                string sqlstr = "UPDATE SiraPtlUserProfile SET SIRA画面表示='";
                // (SIRA画面表示,LEVE画面表示,減メール,満メール,水メール,センサー異常メール,漏えい点検メール,リークメール,データ無しメール) VALUES ('";
                if (bshowsira == true)
                    sqlstr += "1', LEVE画面表示='";
                else
                    sqlstr += "0', LEVE画面表示='";
                if (bshowleve == true)
                    sqlstr += "1', 減メール='";
                else
                    sqlstr += "0', 減メール='";
                if (bsendlow == true)
                    sqlstr += "1', 満メール='";
                else
                    sqlstr += "0', 満メール='";
                if (bsendhi == true)
                    sqlstr += "1', 水メール='";
                else
                    sqlstr += "0', 水メール='";
                if (bsendwtr == true)
                    sqlstr += "1', センサー異常メール='";
                else
                    sqlstr += "0', センサー異常メール='";
                if (bsendsns == true)
                    sqlstr += "1', 漏えい点検メール='";
                else
                    sqlstr += "0', 漏えい点検メール='";
                if (bsendlc4 == true)
                    sqlstr += "1', リークメール='";
                else
                    sqlstr += "0', リークメール='";
                if (bsendlk == true)
                    sqlstr += "1', データ更新無しメール='";
                else
                    sqlstr += "0', データ更新無しメール='";
                if (bsendnodt == true)
                    sqlstr += "1'";
                else
                    sqlstr += "0'";

                sqlstr += " WHERE ユーザー名= '" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //表示可能SS登録
        public static bool RegTable(List<string> chksites, string username)
        {
            bool ret = true;
            try
            {
                string sitelst = "";
                bool firstss = true;
                foreach (string siten in chksites)
                {
                    if (firstss == true)
                    {
                        sitelst += siten;
                        firstss = false;
                    }
                    else
                    {
                        sitelst += "," + siten;
                    }
                }
                string sqlstr = "UPDATE SiraPtlUserProfile SET 表示SS1 = '" + sitelst + "' WHERE ユーザー名 ='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            if (SPUserPrfDT != null)
                return SPUserPrfDT.Rows.Count;
            return 0;
        }

        //参照可能なSKKコードリスト
        public List<string> GetReferrableSkkcodeList(int lineno)
        {
            List<string> codelst = new List<string>();
            try
            {
                string sscodestr = SPUserPrfDT.Rows[lineno][DispSSColNO].ToString().TrimEnd();
                string[] chkdskkcode = new string[0];
                if (sscodestr != "")
                {
                    chkdskkcode = sscodestr.Split(',');
                }
                codelst.AddRange(chkdskkcode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return codelst;
        }

        //指定されたユーザーに対する各SSの表示する・しないチェックリスト用テーブル作成
        public List<string> GetCheckedSSList(string username, int compno)
        {
            CheckedSSList = new List<string>();
            string unamestr = "";
            string[] chkdskkcode = new string[0];
            try
            {
                LeveCompanyProfile lvcprof = new LeveCompanyProfile();
                lvcprof.OpenTableById(compno);

                try
                {
                    unamestr = SPUserPrfDT.Rows[0][DispSSColNO].ToString().TrimEnd();
                    if (unamestr != "")
                    {
                        chkdskkcode = unamestr.Split(',');
                    }
                    CheckedSSList.AddRange(chkdskkcode);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                DataTable DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                System.Data.SqlClient.SqlDataAdapter dAdp;
                string sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable WHERE SKKコード LIKE '" + lvcprof.mrdefcode.Substring(0, 4) + "%'";
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitDT);
#if false //ConnectPort使用の場合
                sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable WHERE SKKコード LIKE '" + GlobalVar.GetDefGwWcSkkcodeByCompname(compname).Substring(0, 4) + "%'";
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                DataTable tempdt = new DataTable();
                dAdp.Fill(tempdt);
                for (int i = 0; i < tempdt.Rows.Count; i++)
                {
                    DataUnitDT.ImportRow(tempdt.Rows[i]);
                }
#endif
                cn.Close();
                List<string> ssnamelst = new List<string>();
                List<string> skkcodelst = new List<string>();
                for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                {
                    skkcodelst.Add(DataUnitDT.Rows[i][0].ToString().TrimEnd());
                    ssnamelst.Add(DataUnitDT.Rows[i][4].ToString().TrimEnd());
                }
                CheckTableDT = new DataTable();
                CheckTableDT.Columns.Add(new DataColumn(stDisp, typeof(bool)));
                CheckTableDT.Columns.Add(new DataColumn(stSiteName, typeof(string)));
                CheckTableDT.Columns.Add(new DataColumn(stSkkcode, typeof(string)));
                string[] ssname = ssnamelst.ToArray();
                string[] skkcode = skkcodelst.ToArray();
                bool found = false;
                for (int i = 0; i < ssname.Length; i++, found = false)
                {
                    DataRow drow = CheckTableDT.NewRow();
                    drow[stSkkcode] = skkcode[i];
                    drow[stSiteName] = ssname[i];
                    for (int j = 0; j < chkdskkcode.Length; j++)
                    {
                        if (skkcode[i] == chkdskkcode[j])
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found == true)
                        drow[stDisp] = true;
                    else
                        drow[stDisp] = false;
                    CheckTableDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return CheckedSSList;
        }

        //チェックリスト用テーブルにあるSKKコードのリストを取得
        public List<string> GetSkkcodeOfCheckTable()
        {
            List<string> skkcdlst = new List<string>();
            try
            {
                for (int i = 0; i < CheckTableDT.Rows.Count; i++)
                {
                    skkcdlst.Add(CheckTableDT.Rows[i][2].ToString().TrimEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return skkcdlst;
        }
        //チェックリスト用テーブルを渡す
        public DataTable GetCheckTable()
        {
            return CheckTableDT;
        }

        //メールアドレス取得
        public string GetMailAddress(int lineno)
        {
            string mladr = "";
            try
            {
                mladr = SPUserPrfDT.Rows[lineno][MailAdrColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }
        //特権ユーザーチェック bool true -- 特権ユーザー　false -- 一般ユーザー
        public bool ChkAdminUser(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][UserTypeColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //特権ユーザーチェック bool true -- 特権ユーザー　false -- 一般ユーザー
        public bool ChkAdminUserByAccname(string accname)
        {
            bool bret = false;
            try
            {
                for (int i = 0; i < SPUserPrfDT.Rows.Count; i++)
                {
                    if (SPUserPrfDT.Rows[i][UserNameColNo].ToString().TrimEnd() == accname)
                    {
                        if ((int)SPUserPrfDT.Rows[i][UserTypeColNo] > 0)
                        {
                            bret = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }


        //SIRA画面表示 bool-true 表示する。
        public bool ChkShowSiraDisp(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][SiraDispColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //在庫画面表示 bool-true 表示する。
        public bool ChkShowLeveDisp(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][LeveDispColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //減メール送信 bool-true送信する
        public bool ChkSendLowMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][LowMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //満メール送信 bool-true送信する
        public bool ChkSendHighMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][HighMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //水警報メール送信 bool true -送信する
        public bool ChkSendWtrMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][WtrMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //センサー異常警報メール送信 bool true -送信する
        public bool ChkSendSnsMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][SnsMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //漏えい点検警報メール送信 bool true -送信する
        public bool ChkSendLc4Mail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][LC4MailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //リーク警報メール送信 bool true -送信する
        public bool ChkSendLeakMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][LeakMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //データ無し警報メール送信 bool true -送信する
        public bool ChkSendNoDataMail(int lineno)
        {
            bool bret = false;
            try
            {
                if ((int)SPUserPrfDT.Rows[lineno][NoDataMailColNo] > 0)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

    }
}