﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace AzureMblApi.Models
{
    public class GlobalVar
    {
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";
        public static bool testmode =false;
        public static string appname = "Rooms";
        public static string appnameeng = "Rooms";
        //public static string ServerPath { get; set; }
        public static int NUMTANK = 20;
        //------ 対象会社名 -------------------
        public const int CompanySKKNo = 0;
        public const int CompanyAikoNo = 1;
        public const int CompanyEneosNo = 2;
        public const int CompanyShowakosanNo = 3;
        public const int CompanyKygnusNo = 4;
        public const int CompanyJxtgNo = 5;
        public static string GetCompanyName(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompanyName;
                case CompanyAikoNo:
                    return AikoCompanyName;
                case CompanyEneosNo:
                    return EneCompanyName;
                case CompanyKygnusNo:
                    return KygCompanyName2;
                case CompanyJxtgNo:
                    return JxtgCompanyName2;
            }
        }

        public static string GetDefGWWcSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompGWUSkkcode;
                case CompanyAikoNo:
                    return AikoCompGWUSkkcode;
                case CompanyEneosNo:
                    return EneCompGWUSkkcode;
            }
        }
        public static string GetDefGwWcSkkcodeByCompname(string compname)
        {
            if (compname == AikoCompanyName)
                return AikoCompGWUSkkcode;
            else if (compname == EneCompanyName)
                return EneCompGWUSkkcode;
            else
                return SKKCompGWUSkkcode;
        }
        public static string GetDefWcSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompMBUSkkcode;
                case CompanyAikoNo:
                    return AikoCompMBUSkkcode;
                case CompanyEneosNo:
                    return EneCompMBUSkkcode;
            }
        }
        public static string GetDefWcSkkcodeByCompname(string compname)
        {
            if (compname == AikoCompanyName)
                return AikoCompMBUSkkcode;
            else if (compname == EneCompanyName)
                return EneCompMBUSkkcode;
            else
                return SKKCompMBUSkkcode;
        }

        public static string GetDefSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSkkcode1;
                case CompanyAikoNo:
                    return AikoDefSkkcode1;
                case CompanyEneosNo:
                    return EneDefSkkcode1;
            }
        }

        public static string GetDefSitename(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSiteName1;
                case CompanyAikoNo:
                    return AikoDefSiteName1;
                case CompanyEneosNo:
                    return EneDefSiteName1;
            }
        }

        public static string MlSysCompanyName = "SKK";
        public static string MlCompanyName = "SKK";
        //------- SKK -------------------------------
        public static string SKKCompanyName = "SKK";
        public static string SKKCompanyName2 = "SKK";
        public static string SKKCompMBUSkkcode = "M099";
        public static string SKKDefSkkcode1 = "M099000001";
        public static string SKKDefSiteName1 = "SKKテスト";
        public static string SKKCompGWUSkkcode = "A099";
        public static string SKKAdminName = "SKKPrimeAdmin";
        public static string SKKSubAdminName = "SKKSubAdmin";
        public static int SkkMaxTankNo = 8;

        public static string SKKCompanyName3 = "SKK";
        public static string SKKMlSysCompanyName = "SKK";
        public static string SKKMlCompanyName = "Skk";
        public static string SKKCompanyFolder = "SkkKaihatsu";
        public static string SKKMlAdr1 = "n-matsuda@showa-kiki.co.jp";

        //-------- 相光石油 -----------------------------
        public static string AikoCompanyName = "AikoSekiyu";
        public static string AikoCompanyName2 = "相光石油";
        public static string AikoCompMBUSkkcode = "M012";
        public static string AikoDefSkkcode1 = "M012000001";
        public static string AikoDefSiteName1 = "大江SS";
        public static string AikoCompGWUSkkcode = "A060";
        public static string AikoSubAdminName = "AikoSubAdmin";
        public static int AikoMaxTankNo = 9;

        public static string AikoCompanyName3 = "相光石油";
        public static string AikoMlSysCompanyName = "相光石油";
        public static string AikoMlCompanyName = "相光石油";
        public static string AikoCompanyFolder = "Test1";

        //-------- エネオス株式会社 -----------------------------
        public static string EneCompanyName = "EneosCorp";
        public static string EneCompanyName2 = "エネオス株式会社";
        public static string EneCompMBUSkkcode = "M013";
        public static string EneDefSkkcode1 = "M013000001";
        public static string EneDefSiteName1 = "青森インターSS";
        public static string EneCompGWUSkkcode = "A099";
        public static string EneSubAdminName = "EneosSubAdmin";
        public static int EneMaxTankNo = 11;

        //-------------- キグナス石油 -------------------------------
        public static string KygCompanyName = "KygnusOil";
        public static string KygCompanyName2 = "エネオス石油販売株式会社";
        public static string KygCompMBUSkkcode = "M00";
        public static string KygDefSkkcode1 = "M000200015";
        public static string KygDefSiteName1 = "セルフ川越的場";
        public static string KygCompGWUSkkcode = "A099";
        public static string KygSubAdminName = "KygnusSubAdmin";
        public static int KygMaxTankNo = 11;

        //-------------- JXTGエネルギー -----------------------------
        public static string JxtgCompanyName = "JXTGEnergy";
        public static string JxtgCompanyName2 = "JXTGエネルギー株式会社";
        public static string JxtgCompMBUSkkcode = "M1";
        public static string JxtgDefSkkcode1 = "M100000001";
        public static string JxtgDefSiteName1 = "DDセルフ赤間SS";
        public static string JxtgCompGWUSkkcode = "A099";
        public static string JxtgSubAdminName = "JxtgSubAdmin";
        public static int JxtgMaxTankNo = 11;

    }



    public enum WrgType
    {
        LowWrg = 0, //減
        HighWrg,  //満
        WtrWrg,    //水
        SnsErrWrg, //センサー異常
        LC4Wrg,    //漏えい点検
        LeakWrg,   //リーク
        NoDataWrg  //データ無し
    }

    public class wrgdata
    {
        public string skkcode { get; set; }
        public string datetime { get; set; }
        public string tankno { get; set; }
        public string wrgtype { get; set; }
        public string wrgtypeeng { get; set; }
        public string onoff { get; set; }
    }

    public class ErrDataDetail
    {
        public string SiteName;    //施設名
        public string Skkcode;     //SKKコード
        public string Companyname;  //会社名
        public int OilTypeno;       //液種名
        public int TankNo;          //タンク番号
        public string DateStr;      //日時
        public int WrgType;         //警報種類
        public int TrgOrRel;        //0:警報解除 1:発生
        public int WtrLvl;          //水位
        public int WtrVol;          //水量
        public string LC4Res;       //漏えい点検結果,判定値

        public ErrDataDetail(string SName, string Scode, string Cname, int Ono, int Tno, string DStr, int WType, int ToR, int Wl, int Wv, string res)
        {
            SiteName = SName;
            Skkcode = Scode;
            Companyname = Cname;
            OilTypeno = Ono;
            TankNo = Tno;
            DateStr = DStr;
            WrgType = WType;
            TrgOrRel = ToR;
            WtrLvl = Wl;
            WtrVol = Wv;
            LC4Res = res;
        }
    }

    public class OilName
    {
        public static string GetOilName(int oiltype)
        {
            string oilname = "";
            switch (oiltype)
            {
                case 1:
                    return "ハイオク";
                case 2:
                    return "レギュラー";
                case 3:
                    return "灯油";
                case 4:
                    return "軽油";
                case 9:
                default:
                    return "廃油";
            }
            return oilname;
        }
        public static string GetOilNameEng(int oiltype)
        {
            string oilname = "";
            switch (oiltype)
            {
                case 1:
                    return "Premium";
                case 2:
                    return "Unleaded";
                case 3:
                    return "Kerosene";
                case 4:
                    return "Diesel";
                case 9:
                default:
                    return "Waste Oil";
            }
            return oilname;
        }
    }

    public class DataTableCtrl
    {
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }
    public class DBCtrl
    {
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.ExecuteNonQuery();
            cn.Close();
        }
    }

    public class ErrTypeName
    {
        public static string GetErrName(int errtype)
        {
            switch (errtype)
            {
                case (int)WrgType.LowWrg:
                    return "減";
                case (int)WrgType.HighWrg:
                    return "満";
                case (int)WrgType.WtrWrg:
                    return "水検知";
                case (int)WrgType.SnsErrWrg:
                    return "センサ異常";
                case (int)WrgType.LC4Wrg:
                    return "漏えい";
                case (int)WrgType.LeakWrg:
                    return "リーク";
                case (int)WrgType.NoDataWrg:
                    return "データ更新異常";
            }
            return " ";
        }
        public static string GetErrNameEng(int errtype)
        {
            switch (errtype)
            {
                case (int)WrgType.LowWrg:
                    return "Low";
                case (int)WrgType.HighWrg:
                    return "Full";
                case (int)WrgType.WtrWrg:
                    return "Water";
                case (int)WrgType.SnsErrWrg:
                    return "Sensor";
                case (int)WrgType.LC4Wrg:
                    return "Leak test";
                case (int)WrgType.LeakWrg:
                    return "Leak";
                case (int)WrgType.NoDataWrg:
                    return "No data";
            }
            return " ";
        }
    }
    //警報文字列取得
    //str --- SSLAN2ステータス 3byte str2 -- SSLANステータス
    public class GetWarning
    {
        public static (string sts, string stseng) GetWrngStr(string str, string str2)
        {
            string wrgstr = "";
            string wrgeng = "";
            byte[] WrgArray = Encoding.ASCII.GetBytes(str);
            byte sts1 = WrgArray[0];
            byte sts2 = WrgArray[1];
            if ((sts1 & 0x10) == 0x10)
            {
                wrgstr += "減,";
                wrgeng += "Low,";
            }
            if ((sts1 & 0x08) == 0x08)
            {
                wrgstr += "満,";
                wrgeng += "Full,";
            }
            if ((sts1 & 0x04) == 0x04)
            {
                wrgstr += "水検知,";
                wrgeng += "Water,";
            }
            if ((sts1 & 0x02) == 0x02)
            {
                wrgstr += "センサ異常,";
                wrgeng += "Sensor,";
            }
            if ((sts1 & 0x01) == 0x01)
            {
                wrgstr += "リーク,";
                wrgeng += "Leak,";
            }
            if ((sts2 & 0x20) == 0x20)
            {
                wrgstr += "下限,";
                wrgeng += "LLim,";
            }
            if ((sts2 & 0x10) == 0x20)
            {
                wrgstr += "漏えい,"; //LC-1
                wrgeng += "Leak test,";
            }
            if ((sts2 & 0x08) == 0x08)
            {
                wrgstr += "漏えい,"; //LC-4
                wrgeng += "Leak test,";
            }
            if ((sts2 & 0x04) == 0x04)
            {
                wrgstr += "漏えい,"; //LC-5
                wrgeng += "Leak test,";
            }
            if ((sts2 & 0x02) == 0x02)
            {
                wrgstr += "漏えい,"; //LC-7
                wrgeng += "Leak test,";
            }
            if ((sts2 & 0x01) == 0x01)
            {
                wrgstr += "漏えい,"; //LC-8
                wrgeng += "Leak test,";
            }
            if (wrgstr.Length > 1)
            {
                wrgstr = wrgstr.Substring(0, wrgstr.Length - 1); //最後の","をとる
                wrgeng = wrgeng.Substring(0, wrgeng.Length - 1); //最後の","をとる
            }
            if ((wrgstr.Length == 0) && (str2 != "0"))
            {
                switch (str2)
                {
                    case "1":
                        wrgstr = "満";
                        wrgeng = "Full";
                        break;
                    case "2":
                        //wrgstr = "減";
                        //wrgeng = "Low";
                        break;
                    case "3":
                        wrgstr = "水検知";
                        wrgeng = "Water";
                        break;
                    case "4":
                        wrgstr = "満, 水検知";
                        wrgeng = "Full,Water";
                        break;
                    case "5":
                        //wrgstr = "減, 水検知";
                        //wrgeng = "Low,Water";
                        wrgstr = "水検知";
                        wrgeng = "Water";
                        break;
                }
            }
            if (wrgstr == "")
            {
                wrgstr = "通常";
                wrgeng = "no error";
            }
            return (wrgstr, wrgeng);
        }
    }


}