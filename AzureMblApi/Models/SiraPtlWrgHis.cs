﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace AzureMblApi.Models
{
    public class SiraPtlWrgHis
    {
        private DataTable SPWrgHisDt;
        private DataTable WtrWrgStsDt;          //発生中水警報状況
        private DataTable Lc4WrgStsDt;          //発生中漏えい警報状況
        private DataTable LkWrgStsDt;           //発生中リーク警報状況
        private DataTable SnsWrgStsDt;          //発生中液面センサー異常状況
        private DataTable NoDataStsDt;          //発生中データ無し警報

        private DataTable WtrWrgStsDispDt;      //発生中水警報状況      (画面表示用)
        private DataTable Lc4WrgStsDispDt;      //発生中漏えい警報状況      (画面表示用)
        private DataTable LkWrgStsDispDt;       //発生中リーク警報状況      (画面表示用)
        private DataTable SnsWrgStsDispDt;      //発生中液面センサー異常状況(画面表示用)
        private DataTable NoDataStsDispDt;      //発生中データ無し警報      (画面表示用)

        private DataTable WrgHistoryDt;
        private DataTable WrgHistoryDispDt;

        private const int SiteNameColNo = 0;    //施設名
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int CompNameColNo = 2;    //会社名
        private const int OilTypeColNo = 3;   //液種名
        private const int TankNoColNo = 4;    //タンク番号
        private const int WrgDateColNo = 5;   //日時
        private const int WrgTypeColNo = 6;   //警報種類
        private const int TrgOrRelColNo = 7;   //0:警報解除 1:発生
        private const int WtrLvlColNo = 8;    //水位
        private const int WtrVolColNo = 9;    //水量
        private const int LC4ResColNo = 10;   //漏えい点検結果,判定値

        protected string companyname;

        //Constructer
        public SiraPtlWrgHis()
        {
            DataTableCtrl.InitializeTable(SPWrgHisDt);
        }

        //指定された会社名に対するテーブル読み込み
        public void OpenTable(string cmpname)
        {
            try
            {
                companyname = cmpname;
                string sqlstr;
                if (companyname == "*")
                    sqlstr = "SELECT * FROM SiraPtlWrgHis";
                else
                    sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE 会社名= (N'" + companyname + "')";
                DataTableCtrl.InitializeTable(SPWrgHisDt);
                SPWrgHisDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPWrgHisDt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードの指定時刻以降のテーブル読み込み
        public void OpenTableBySkkcodeAndDate(string skkcode, DateTime dt)
        {
            try
            {
                string sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE ";
                if (skkcode.Length >= 10)
                {
                    sqlstr += "SKKコード= '" + skkcode.Substring(0, 10) + "' AND 日時 >= '";
                }
                else
                {
                    sqlstr += "SKKコード LIKE '" + skkcode + "%' AND 日時 >= '";
                }
                sqlstr += dt.ToString("yyyyMMddHHmm") + "' ORDER BY 日時 DESC";
                DataTableCtrl.InitializeTable(SPWrgHisDt);
                SPWrgHisDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPWrgHisDt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public int GetNumOfRecord()
        {
            if (SPWrgHisDt != null)
                return SPWrgHisDt.Rows.Count;
            return 0;
        }

        //指定行、SKKコードを取り出す
        public string GetSkkcodeByLine(int lineno)
        {
            string skkcode = "";
            try
            {
                skkcode = SPWrgHisDt.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return skkcode;
        }

        //指定行、SS名を取り出す
        public string GetSiteNameByLine(int lineno)
        {
            string ssname = "";
            try
            {
                ssname = SPWrgHisDt.Rows[lineno][SiteNameColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return ssname;
        }
        //指定行、class wrgdataを返す
        public wrgdata GetWrgDatByLine(int lineno)
        {
            wrgdata wgdat = new wrgdata();
            try
            {
                wgdat.skkcode = SPWrgHisDt.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
                wgdat.datetime = SPWrgHisDt.Rows[lineno][WrgDateColNo].ToString().TrimEnd();
                wgdat.tankno = SPWrgHisDt.Rows[lineno][TankNoColNo].ToString().TrimEnd();
                int errtype;
                bool res = int.TryParse(SPWrgHisDt.Rows[lineno][WrgTypeColNo].ToString().TrimEnd(), out errtype);
                if( res == true )
                {
                    wgdat.wrgtype = ErrTypeName.GetErrName(errtype);
                    wgdat.wrgtypeeng = ErrTypeName.GetErrNameEng(errtype);
                }
                wgdat.onoff = SPWrgHisDt.Rows[lineno][TrgOrRelColNo].ToString().TrimEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return wgdat;
        }

        //警報履歴書き込み
        public static void WriteWrgHistory(ErrDataDetail edl)
        {
            try
            {
                string sqlstr = "INSERT INTO SiraPtlWrgHis (施設名,SKKコード,会社名,液種,タンク番号,日時,警報種類,発生,水位,水量,漏えい点検結果) VALUES (N'" + edl.SiteName + "','" + edl.Skkcode + "',N'" + edl.Companyname + "','" + edl.OilTypeno.ToString() + "','" + edl.TankNo.ToString() + "','" + edl.DateStr + "','" + edl.WrgType.ToString() + "','" + edl.TrgOrRel.ToString() + "','" + edl.WtrLvl.ToString() + "','" + edl.WtrVol.ToString() + "','" + edl.LC4Res + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードの現在発生中の各警報テーブル読み込み
        public void OpenCurWrgBySkkcode(string skkcode)
        {
            try
            {
                //水警報テーブル
                int wrgno = (int)WrgType.WtrWrg;
                string sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 警報種類=" + wrgno.ToString();
                DataTableCtrl.InitializeTable(WtrWrgStsDt);
                WtrWrgStsDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, WtrWrgStsDt);

                //漏えい監視警報
                wrgno = (int)WrgType.LC4Wrg;
                sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 警報種類=" + wrgno.ToString();
                DataTableCtrl.InitializeTable(Lc4WrgStsDt);
                Lc4WrgStsDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, Lc4WrgStsDt);

                //LkWrgStsDt;           //発生中リーク警報状況
                wrgno = (int)WrgType.LeakWrg;
                sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 警報種類=" + wrgno.ToString();
                DataTableCtrl.InitializeTable(LkWrgStsDt);
                LkWrgStsDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, LkWrgStsDt);

                //SnsWrgStsDt;          //発生中液面センサー異常状況
                wrgno = (int)WrgType.SnsErrWrg;
                sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 警報種類=" + wrgno.ToString();
                DataTableCtrl.InitializeTable(SnsWrgStsDt);
                SnsWrgStsDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SnsWrgStsDt);

                //NoDataStsDt;          //発生中データ無し警報
                wrgno = (int)WrgType.NoDataWrg;
                sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 警報種類=" + wrgno.ToString();
                DataTableCtrl.InitializeTable(NoDataStsDt);
                NoDataStsDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, NoDataStsDt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //画面表示用の警報テーブルを作成
        public void CreateCurWrgStsDisplayTable()
        {
            try
            {
                DataTableCtrl.InitializeTable(WtrWrgStsDt);
                WtrWrgStsDispDt = new DataTable();
                string[] columnlst = { "施設名", "施設コード", "タンク", "液種", "警報日時", "水位", "水量", "状況" };
                WtrWrgStsDispDt.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

                DataTableCtrl.InitializeTable(Lc4WrgStsDt);
                Lc4WrgStsDispDt = new DataTable();
                string[] columnlst2 = { "施設名", "施設コード", "タンク", "液種", "警報日時", "変化量", "状況", "判定" };
                Lc4WrgStsDispDt.Columns.AddRange(columnlst2.Select(n => new DataColumn(n)).ToArray());

                DataTableCtrl.InitializeTable(LkWrgStsDt);
                LkWrgStsDispDt = new DataTable();
                string[] columnlst3 = { "施設名", "施設コード", "タンク", "液種", "警報日時", "状況" };
                LkWrgStsDispDt.Columns.AddRange(columnlst3.Select(n => new DataColumn(n)).ToArray());

                DataTableCtrl.InitializeTable(SnsWrgStsDt);
                SnsWrgStsDispDt = new DataTable();
                SnsWrgStsDispDt.Columns.AddRange(columnlst3.Select(n => new DataColumn(n)).ToArray());

                DataTableCtrl.InitializeTable(NoDataStsDt);
                NoDataStsDispDt = new DataTable();
                NoDataStsDispDt.Columns.AddRange(columnlst3.Select(n => new DataColumn(n)).ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //画面表示用の警報テーブルの項目追加
        public void AddCurWrgStsDisplayTable(SiraPtlSSInf ssinf)
        {
            try
            {
                try
                {
                    //水テーブル追加
                    for (int i = 0; i < WtrWrgStsDt.Rows.Count; i++)
                    {
                        if ((ssinf.ChkWtrWrgSts(0, (int)WtrWrgStsDt.Rows[i][TankNoColNo]) == true) && ((int)WtrWrgStsDt.Rows[i][TrgOrRelColNo] == 1))
                        {
                            DataRow drow = WtrWrgStsDispDt.NewRow();
                            //drow["施設名"] = WtrWrgStsDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                            drow["施設名"] = ssinf.GetSiteNameByLine(0);
                            drow["施設コード"] = WtrWrgStsDt.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            drow["タンク"] = ((int)WtrWrgStsDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                            int oltype = (int)WtrWrgStsDt.Rows[i][OilTypeColNo];
                            drow["液種"] = OilName.GetOilName(oltype);
                            string datestr = WtrWrgStsDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                            drow["警報日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            int wtrlvl = (int)WtrWrgStsDt.Rows[i][WtrLvlColNo];
                            drow["水位"] = wtrlvl.ToString();
                            int wtrvol = (int)WtrWrgStsDt.Rows[i][WtrVolColNo];
                            drow["水量"] = wtrvol.ToString();
                            drow["状況"] = "水検知";
                            WtrWrgStsDispDt.Rows.Add(drow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                try
                {
                    for (int i = 0; i < Lc4WrgStsDt.Rows.Count; i++)
                    {
                        //漏えい監視
                        if ((ssinf.ChkLc4WrgSts(0, (int)Lc4WrgStsDt.Rows[i][TankNoColNo]) == true) && ((int)Lc4WrgStsDt.Rows[i][TrgOrRelColNo] == 1))
                        {
                            DataRow drow = Lc4WrgStsDispDt.NewRow();
                            //drow["施設名"] = Lc4WrgStsDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                            drow["施設名"] = ssinf.GetSiteNameByLine(0);
                            drow["施設コード"] = Lc4WrgStsDt.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            drow["タンク"] = ((int)Lc4WrgStsDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                            int oltype = (int)Lc4WrgStsDt.Rows[i][OilTypeColNo];
                            drow["液種"] = OilName.GetOilName(oltype);
                            string datestr = Lc4WrgStsDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                            drow["警報日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            string rsltstr = Lc4WrgStsDt.Rows[i][LC4ResColNo].ToString().TrimEnd();
                            drow["変化量"] = rsltstr = rsltstr.Substring(1);
                            drow["判定"] = "不合格";
                            Lc4WrgStsDispDt.Rows.Add(drow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                try
                {
                    for (int i = 0; i < LkWrgStsDt.Rows.Count; i++)
                    {
                        //漏えい監視
                        if ((ssinf.ChkLkWrgSts(0, (int)LkWrgStsDt.Rows[i][TankNoColNo]) == true) && ((int)LkWrgStsDt.Rows[i][TrgOrRelColNo] == 1))
                        {
                            DataRow drow = LkWrgStsDispDt.NewRow();
                            //drow["施設名"] = LkWrgStsDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                            drow["施設名"] = ssinf.GetSiteNameByLine(0);
                            drow["施設コード"] = LkWrgStsDt.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            drow["タンク"] = ((int)LkWrgStsDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                            int oltype = (int)LkWrgStsDt.Rows[i][OilTypeColNo];
                            drow["液種"] = OilName.GetOilName(oltype);
                            string datestr = LkWrgStsDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                            drow["警報日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            drow["状況"] = "リークセンサー作動";
                            LkWrgStsDispDt.Rows.Add(drow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                try
                {
                    //発生中液面センサー異常状況
                    for (int i = 0; i < SnsWrgStsDt.Rows.Count; i++)
                    {
                        if ((ssinf.ChkSnsWrgSts(0, (int)SnsWrgStsDt.Rows[i][TankNoColNo]) == true) && ((int)SnsWrgStsDt.Rows[i][TrgOrRelColNo] == 1))
                        {
                            DataRow drow = SnsWrgStsDispDt.NewRow();
                            //drow["施設名"] = SnsWrgStsDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                            drow["施設名"] = ssinf.GetSiteNameByLine(0);
                            drow["施設コード"] = SnsWrgStsDt.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            drow["タンク"] = ((int)SnsWrgStsDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                            int oltype = (int)SnsWrgStsDt.Rows[i][OilTypeColNo];
                            drow["液種"] = OilName.GetOilName(oltype);
                            string datestr = SnsWrgStsDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                            drow["警報日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            drow["状況"] = "センサー異常";
                            SnsWrgStsDispDt.Rows.Add(drow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                try
                {

                    //発生中データ無し警報
                    for (int i = 0; i < NoDataStsDt.Rows.Count; i++)
                    {
                        if ((ssinf.ChkNoDataWrgSts(0, (int)NoDataStsDt.Rows[i][TankNoColNo]) == true) && ((int)NoDataStsDt.Rows[i][TrgOrRelColNo] == 1))
                        {
                            DataRow drow = NoDataStsDispDt.NewRow();
                            //drow["施設名"] = NoDataStsDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                            drow["施設名"] = ssinf.GetSiteNameByLine(0);
                            drow["施設コード"] = NoDataStsDt.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            drow["タンク"] = ((int)NoDataStsDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                            int oltype = (int)NoDataStsDt.Rows[i][OilTypeColNo];
                            drow["液種"] = OilName.GetOilName(oltype);
                            string datestr = NoDataStsDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                            drow["警報日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            drow["状況"] = "データ無し";
                            NoDataStsDispDt.Rows.Add(drow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
        //発生中水警報状況      (画面表示用)
        public DataTable GetWtrWrgTbl()
        {
            if (WtrWrgStsDispDt.Rows.Count == 0)
            {
                DataRow drow = WtrWrgStsDispDt.NewRow();
                drow["施設名"] = "水警報発生なし";
                drow["施設コード"] = " ";
                drow["タンク"] = " ";
                drow["液種"] = " ";
                drow["警報日時"] = " ";
                drow["水位"] = " ";
                drow["水量"] = " ";
                drow["状況"] = " ";
                WtrWrgStsDispDt.Rows.Add(drow);
            }
            return WtrWrgStsDispDt;
        }

        //発生中漏えい警報状況      (画面表示用)
        public DataTable GetLc4WrgTbl()
        {
            if (Lc4WrgStsDispDt.Rows.Count == 0)
            {
                DataRow drow = Lc4WrgStsDispDt.NewRow();
                drow["施設名"] = "漏えい警報発生なし";
                drow["施設コード"] = " ";
                drow["タンク"] = " ";
                drow["液種"] = " ";
                drow["警報日時"] = " ";
                drow["変化量"] = " ";
                drow["判定"] = " ";
                Lc4WrgStsDispDt.Rows.Add(drow);
            }
            return Lc4WrgStsDispDt;
        }
        //発生中リーク警報状況      (画面表示用)
        public DataTable GetLkWrgTbl()
        {
            if (LkWrgStsDispDt.Rows.Count == 0)
            {
                DataRow drow = LkWrgStsDispDt.NewRow();
                drow["施設名"] = "異常警報発生なし";
                drow["施設コード"] = " ";
                drow["タンク"] = " ";
                drow["液種"] = " ";
                drow["警報日時"] = " ";
                drow["状況"] = " ";
                LkWrgStsDispDt.Rows.Add(drow);
            }
            return LkWrgStsDispDt;
        }
        //発生中液面センサー異常状況(画面表示用)
        public DataTable GetSnsWrgTbl()
        {
            if (SnsWrgStsDispDt.Rows.Count == 0)
            {
                DataRow drow = SnsWrgStsDispDt.NewRow();
                drow["施設名"] = "異常警報発生なし";
                drow["施設コード"] = " ";
                drow["タンク"] = " ";
                drow["液種"] = " ";
                drow["警報日時"] = " ";
                drow["状況"] = " ";
                SnsWrgStsDispDt.Rows.Add(drow);
            }
            return SnsWrgStsDispDt;
        }
        //発生中データ無し警報      (画面表示用)
        public DataTable GetNoDataTbl()
        {
            if (NoDataStsDispDt.Rows.Count == 0)
            {
                DataRow drow = NoDataStsDispDt.NewRow();
                drow["施設名"] = "データ無し警報発生なし";
                drow["施設コード"] = " ";
                drow["タンク"] = " ";
                drow["液種"] = " ";
                drow["警報日時"] = " ";
                drow["状況"] = " ";
                NoDataStsDispDt.Rows.Add(drow);
            }
            return NoDataStsDispDt;
        }

        //指定SKKコードの指定月の警報履歴をテーブル読み出し
        public void OpenWrgHistoryBySkkcodeMonth(string skkcode, DateTime dt)
        {
            try
            {
                //警報テーブル
                string sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 日時 LIKE '" + dt.ToString("yyyyMM") + "%' ORDER BY 日時 DESC";
                //string sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE SKKコード= '" + skkcode + "' AND 日時 LIKE '" + dt.ToString("yyyyMM") + "%' COLLATE JAPANESE_CS_AS ORDER BY 日時 DESC";
                DataTableCtrl.InitializeTable(WrgHistoryDt);
                WrgHistoryDt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, WrgHistoryDt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //警報履歴をテーブル作成
        public void CreateWrgHistoryDisplayTable()
        {
            try
            {
                DataTableCtrl.InitializeTable(WrgHistoryDispDt);
                WrgHistoryDispDt = new DataTable();
                string[] columnlst = { "施設名", "タンク", "液種", "日時", "事象" };
                WrgHistoryDispDt.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
        //警報履歴表示用のテーブルを作成
        public void AddWrgHistoryDisplayTable(SiraPtlSSInf ssinf)
        {
            try
            {
                for (int i = 0; i < WrgHistoryDt.Rows.Count; i++)
                {
                    DataRow drow = WrgHistoryDispDt.NewRow();
                    //drow["施設名"] = WrgHistoryDt.Rows[i][SiteNameColNo].ToString().TrimEnd();
                    drow["施設名"] = ssinf.GetSiteNameByLine(0);
                    int errtype = (int)WrgHistoryDt.Rows[i][WrgTypeColNo];
                    if (errtype == (int)WrgType.NoDataWrg)
                        drow["タンク"] = "";
                    else
                        drow["タンク"] = ((int)WrgHistoryDt.Rows[i][TankNoColNo]).ToString().TrimEnd();
                    int oltype = (int)WrgHistoryDt.Rows[i][OilTypeColNo];
                    drow["液種"] = OilName.GetOilName(oltype);
                    string datestr = WrgHistoryDt.Rows[i][WrgDateColNo].ToString().TrimEnd();
                    drow["日時"] = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                    int erron = (int)WrgHistoryDt.Rows[i][TrgOrRelColNo];
                    if (erron > 0)
                    {
                        if (errtype == (int)WrgType.WtrWrg)
                        {
                            int wtrlvl = (int)WrgHistoryDt.Rows[i][WtrLvlColNo];
                            wtrlvl = (wtrlvl + 50) / 100;
                            drow["事象"] = ErrTypeName.GetErrName(errtype) + "　発生   水位 " + wtrlvl.ToString() + "mm";
                        }
                        else
                        {
                            drow["事象"] = ErrTypeName.GetErrName(errtype) + "　発生";
                        }
                    }
                    else
                        drow["事象"] = ErrTypeName.GetErrName(errtype) + "　解除";
                    WrgHistoryDispDt.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //警報履歴表示用のテーブルを渡す
        public DataTable GetWrgHisDispTable()
        {
            return WrgHistoryDispDt;
        }

        public static string GetWrgHisHeader()
        {
            string header = "";
            header += "施設名,タンク番号,液種,日時,警報\r\n";
            return header;
        }

        //警報履歴表示用のテーブルより文字列をCSV形式で作成し渡す
        public string GetWrgHisDispString()
        {
            string wrgstr = "";
            try
            {
                for (int i = 0; i < WrgHistoryDispDt.Rows.Count; i++)
                {
                    wrgstr += WrgHistoryDispDt.Rows[i][0].ToString().TrimEnd() + ","; //施設名
                    wrgstr += WrgHistoryDispDt.Rows[i][1].ToString().TrimEnd() + ","; //タンク
                    wrgstr += WrgHistoryDispDt.Rows[i][2].ToString().TrimEnd() + ","; //液種
                    wrgstr += WrgHistoryDispDt.Rows[i][3].ToString().TrimEnd() + ","; //日時
                    wrgstr += WrgHistoryDispDt.Rows[i][4].ToString().TrimEnd(); //事象
                    wrgstr += "\r\n";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return wrgstr;
        }
    }
}