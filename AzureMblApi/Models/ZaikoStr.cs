﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureMblApi.Models
{
    public class ZaikoStr
    {
        private const int NUMTANK = 20; //最大タンク数

        private const int OFFSHEAD = 10; //データヘッダーサイズ
        private const int OFFSHEADCNPORT = 10; //ConnectPort時のヘッダーデータサイズ
        private const int OFFSTANK = 42; //1タンクあたりのサイズ
        private const int OFFSSTAT = 2; //SS-LAN状況コードまでのオフセット
        private const int OFFSSTAT1 = 14; //状況コード1までのオフセット
        private const int OFFSZAIKO = 6;  //在庫量までのオフセット
        private const int OFFSOTYPE = 3;  //液種タイプまでのオフセット
        private const int OFFSLNKINF = 17; //連結情報までのオフセット
        private const int OFFSTEMP = 25;  //温度情報までのオフセット
        private const int OFFSCAPA = 30;  //タンク容量までのオフセット
        private const int OFFSLOWWG = 36; //減警報値までのオフセット
        private const int OFFSTANK2 = 68;
        private const int OFFSWTRLVL = 42; //水位までのオフセット
        private const int OFFSWTRVOL = 48; //水量までのオフセット
        private const int OFFSOILLVL = 54; //液位情報までのオフセット
        private const int OFFSDLVVOL = 60; //荷卸し情報までのオフセット
        private const int OFFSSALESVOL = 62; //販売情報までのオフセット
        private const string WTRDTMARK = "p"; //水情報ありのマーカー

        protected string zstr;
        public DateTime timestmp;       //時刻
        public TimeSpan timespn;        //現在時刻との差異
        //public string timespnstr;       //現在時刻との差異文字列
        public bool bnodaterr;
        public List<string> tanknolst; //タンク番号
        public List<string> lqtypelst; //液種
        public List<string> lqtypelst2; //液種
        public List<string> vollst; //液量
        public List<string> sslanstat; //SS-LANステータス
        public List<string> sslan2stat; //SS-LAN2ステータス
        public List<string> templst;  //温度情報
        public List<string> capalst; //タンク容量
        public List<string> lowlmtlst;   //減容量
        public List<string> wtrlvllst;   //水位
        public List<string> wtrvollst;      //水量
        public List<string> oillvllst;      //液位情報
        public List<string> dlvvollst;      //荷卸量情報
        public List<string> salesvollst;    //販売量情報
        public bool bwtrinf;             //水情報有り無し
        public int numtank;              //タンク数

        public ZaikoStr(string data) //constructor
        {
            zstr = data;
            //timespnstr = "";       //現在時刻との差異文字列
            bnodaterr = false;
            bwtrinf = false;
            numtank = 0;
            tanknolst = new List<string>();
            lqtypelst = new List<string>();
            lqtypelst2 = new List<string>();
            vollst = new List<string>();
            capalst = new List<string>();
            sslanstat = new List<string>();
            sslan2stat = new List<string>();
            lowlmtlst = new List<string>();
            templst = new List<string>();
            wtrlvllst = new List<string>();
            wtrvollst = new List<string>();
            oillvllst = new List<string>();
            dlvvollst = new List<string>();
            salesvollst = new List<string>();
        }

        public void Analyze()
        {
            int i;
            int tankoffs = OFFSTANK;  //タンク間の長さ
            int tankheads = OFFSHEAD;
            //bool bcnctport = false; 
            string datestr = "20" + zstr.Substring(0, 10);
            //if (datestr.Substring(0, 2) != "20")
            //{
            //bcnctport = true;       //ConnectPortの書式
            //datestr = "20" + datestr.Substring(0, 10);
            //}
            datestr = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
            timestmp = DateTime.Parse(datestr);
            DateTime dtnow = DateTime.Now.ToLocalTime();
            timespn = dtnow - timestmp;
            if (timespn.TotalMinutes > 60)
            {
                bnodaterr = true;
            }
            //if (bcnctport == true)      //ConnectPort時のヘッダー調整
            //{
            //    tankheads = OFFSHEADCNPORT;
            //}
            string fstbyte = zstr.Substring(tankheads, 1);
            if (fstbyte == WTRDTMARK)
            {
                bwtrinf = true;
                tankoffs = OFFSTANK2;
            }
            for (i = 0; i < NUMTANK; i++)       //最大タンク数分ループ
            {
                if ((tankheads + tankoffs * (i + 1)) > zstr.Length)  //これ以上タンクの情報がない場合はブレイク
                {
                    break;
                }
                //タンク番号取得
                if (i == 0)
                    tanknolst.Add(zstr.Substring(tankheads + tankoffs * i + 1, 1));
                else
                    tanknolst.Add(zstr.Substring(tankheads + tankoffs * i, 2));

                //液種
                lqtypelst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSOTYPE, 1));
                lqtypelst2.Add(zstr.Substring(tankheads + tankoffs * i + OFFSOTYPE + 1, 2));

                //在庫量, タンク容量取得
                vollst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSZAIKO, 6));

                //SS-LANステータス
                sslanstat.Add(zstr.Substring(tankheads + tankoffs * i + OFFSSTAT, 1));

                //SS-LAN2ステータス
                sslan2stat.Add(zstr.Substring(tankheads + tankoffs * i + OFFSSTAT1, 3));

                //温度情報
                templst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSTEMP, 5));

                //容量
                capalst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSCAPA, 6));

                //減警報値
                lowlmtlst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSLOWWG, 6));

                if (bwtrinf == true)
                {
                    //水位
                    wtrlvllst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSWTRLVL, 6));
                    //水量
                    wtrvollst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSWTRVOL, 6));
                    //液位情報情報
                    oillvllst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSOILLVL, 6));
                    //荷卸し情報
                    dlvvollst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSDLVVOL, 2));
                    //販売情報
                    salesvollst.Add(zstr.Substring(tankheads + tankoffs * i + OFFSSALESVOL, 6));
                }
                else
                {
                    //水位
                    wtrlvllst.Add("     0");
                    //水量
                    wtrvollst.Add("     0");
                    //液位情報情報
                    oillvllst.Add("     0");
                    //荷卸し情報
                    dlvvollst.Add(" 0");
                    //販売情報
                    salesvollst.Add("      ");
                }

            }
            numtank = i;
        }

    }
}