﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Diagnostics;

namespace AzureMblApi.Models
{
    public class SendMail
    {
        public void SendMailExec(string mlmsg, string toadr, string lang )
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                //内容先頭
                Execute(mlmsg, toadr, dt.ToString("yyyy年MM月dd日HH時mm分"),lang).Wait();
                Console.WriteLine("Send mail to " + toadr + mlmsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected async Task Execute(string mlmsg, string toadr, string datestr, string lang)
        {
            try
            {
                //ローカルでテスト
                var apiKey = "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";
                //サーバーデプロイ時
                //var apiKey = System.Environment.GetEnvironmentVariable("SENDGRID_APIKEY");

                var client = new SendGridClient(apiKey);
                string addr = GlobalVar.appnameeng + "@skkatgs.jp";
                var from = new EmailAddress(addr, "昭和機器工業株式会社 メールシステム");
                var subject = "PINコード";
                if (lang.StartsWith("Eng") == true)
                {
                    from = new EmailAddress(GlobalVar.appnameeng + "@skkatgs.jp", "Showa Kiki Kogyo Mail System");
                    subject = "PIN CODE";
                }

                EmailAddress to = new EmailAddress(toadr, GlobalVar.appname + "お客様");
                if (lang.StartsWith("Eng") == true)
                {
                    to = new EmailAddress(toadr, GlobalVar.appnameeng + "customer");
                }
                var plainTextContent = mlmsg;
                mlmsg = mlmsg.Replace("\r\n", "<br>");
                var htmlContent = "<p style=\"font-family:Meiryo\">" + mlmsg + "</p>";

                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = await client.SendEmailAsync(msg).ConfigureAwait(false);

                Debug.WriteLine(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

    }
}